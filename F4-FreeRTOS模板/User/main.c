/*
 *                        _oo0oo_
 *                       o8888888o
 *                       88" . "88
 *                       (| -_- |)
 *                       0\  =  /0
 *                     ___/`---'\___
 *                   .' \\|     |// '.
 *                  / \\|||  :  |||// \
 *                 / _||||| -:- |||||- \
 *                |   | \\\  - /// |   |
 *                | \_|  ''\---/''  |_/ |
 *                \  .-\__  '-'  ___/-. /
 *              ___'. .'  /--.--\  `. .'___
 *           ."" '<  `.___\_<|>_/___.' >' "".
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /
 *      =====`-.____`.___ \_____/___.-`___.-'=====
 *                        `=---='
 *
 *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *            佛祖保佑     永不宕机     永无BUG
 *
 *        佛曰:
 *                写字楼里写字间，写字间里程序员；
 *                程序人员写程序，又拿程序换酒钱。
 *                酒醒只在网上坐，酒醉还来网下眠；
 *                酒醉酒醒日复日，网上网下年复年。
 *                但愿老死电脑间，不愿鞠躬老板前；
 *                奔驰宝马贵者趣，公交自行程序员。
 *                别人笑我忒疯癫，我笑自己命太贱；
 *                不见满街漂亮妹，哪个归得程序员？
 */

/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 20:34:25
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\main.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\main.c
 * @Description:
 */
/*包含所有头文件*/
#include "main.h"

/* 任务句柄 */
static TaskHandle_t AppTaskCreate_Handle = NULL;
TaskHandle_t Test_Task_Handle = NULL;
TaskHandle_t KEY_Task_Handle = NULL;
TaskHandle_t Test_date_Handle = NULL;

/* 队列句柄 */
QueueHandle_t Tof_Data_Queue = NULL;
QueueHandle_t Imu_Data_Queue = NULL;
QueueHandle_t Tw1000_Data_Queue = NULL;
QueueHandle_t Oled_Data_Queue = NULL;
QueueHandle_t CT_Queue = NULL;
QueueHandle_t Remoter_Queue = NULL;

static void AppTaskCreate(void);
static void BSP_Init(void);

int main(void) {
  BaseType_t xReturn = pdPASS;

  /* 外设初始化*/
  BSP_Init();

  /* 创建开始任务*/
  xReturn =
      xTaskCreate((TaskFunction_t)AppTaskCreate, (const char *)"AppTaskCreate",
                  (uint16_t)512, (void *)NULL, (UBaseType_t)1,
                  (TaskHandle_t *)&AppTaskCreate_Handle);
  if (pdPASS == xReturn)
    vTaskStartScheduler();
  else
    return -1;

  while (1)
    ;
}
/**********************
 * 任务创建函数
 * 功能：创建多个任务和消息队列，以实现系统中各个模块的数据处理和交互。
 * 参数：无
 * 返回值：无
 **********************/
static void AppTaskCreate(void) {
  BaseType_t xReturn = pdPASS;

  taskENTER_CRITICAL();  // 进入临界段，保护队列创建过程

  /* 创建系统中使用的多个消息队列 */
  // 创建用于接收摄像头模块数据的消息队列
  Tof_Data_Queue = xQueueCreate(10, sizeof(tof_data_t));
  if (NULL != Tof_Data_Queue) {
    printf("创建消息队列成功:Tof_Data_Queue\n");
  }

  // 创建用于接收IMU数据的消息队列
  Imu_Data_Queue = xQueueCreate(10, sizeof(imu_data_t));
  if (NULL != Imu_Data_Queue) {
    printf("创建消息队列成功:Imu_Data_Queue\n");
  }

  // 创建用于接收TW1000数据的消息队列
  Tw1000_Data_Queue = xQueueCreate(10, sizeof(tw1000_data_t));
  if (NULL != Tw1000_Data_Queue) {
    printf("创建消息队列成功:Tw1000_Data_Queue\n");
  }

  // 创建用于显示数据的消息队列
  Oled_Data_Queue = xQueueCreate(10, sizeof(oled_data_t));
  if (NULL != Oled_Data_Queue) {
    printf("创建消息队列成功:Oled_Data_Queue\n");
  }

  // 创建用于设置期望坐标的队列
  CT_Queue = xQueueCreate(1, sizeof(CT_data_t));
  if (NULL != CT_Queue) {
    printf("创建消息队列成功:CT_Queue\n");
  }

  // 创建遥控器数据队列
  Remoter_Queue = xQueueCreate(1, sizeof(remote_info_t));
  if (NULL != Remoter_Queue) {
    printf("创建消息队列成功:Remoter_Queue\n");
  }

  /* 创建测试任务 */
  xReturn = xTaskCreate((TaskFunction_t)Test_Task, (const char *)"Test_Task",
                        (uint16_t)256, (void *)NULL, (UBaseType_t)5,
                        (TaskHandle_t *)&Test_Task_Handle);
  if (pdPASS == xReturn) printf("Test_Task OK!\r\n");

  // 创建测试数据处理任务
  xReturn = xTaskCreate((TaskFunction_t)Test_date, (const char *)"Test_date",
                        (uint16_t)256, (void *)NULL, (UBaseType_t)4,
                        (TaskHandle_t *)&Test_date_Handle);
  if (pdPASS == xReturn) printf("Test_date OK!\r\n");

  // 创建按键处理任务
  xReturn = xTaskCreate((TaskFunction_t)KEY_Task, (const char *)"KEY_Task",
                        (uint16_t)128, (void *)NULL, (UBaseType_t)2,
                        (TaskHandle_t *)&KEY_Task_Handle);
  if (pdPASS == xReturn) printf("KEY_Task OK!\r\n");

  // 删除创建任务的初始任务
  vTaskDelete(AppTaskCreate_Handle);
  taskEXIT_CRITICAL();  // 退出临界段，结束队列创建过程
}

/***********************************************************************
 * 中断分组及外设初始化
 *********************************************************************/
static void BSP_Init(void) {
  /* 中断分组 */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

  LED_GPIO_Config();

  Debug_USART_Config(115200);
  USART2_Init(115200);
  USART3_Init(115200);
  UART4_Init(115200);
  USART5_init(115200);
  TIM3_PWM_Init(20000 - 1, 84 - 1);
  TIM14_PWM_Init(20000 - 1, 84 - 1);
  TIM8_PWM_Init(20000 - 1, 168 - 1);
  // TIM5_Int_Init(8400 - 1);
  // TIM7_Init_Clock(2000 - 1, 84 - 1);
  IIC_Init();
  OLED_Init();
  Key_GPIO_Config();
  motor_init();

  // PID init
  Pid_Config_t motor_pid_init_wt1000_x = INIT_PID_CONFIG(
      80.0f, 0.1f, 1.0f, 1.5f, 800.0f, 15000.0f,
      (pid_improvement_e)(PID_Trapezoid_Intergral | PID_Integral_Limit |
                          PID_Derivative_On_Measurement),
      (pid_mode_e)PID_POSITION);
  Pid_Config_t motor_pid_init_wt1000_y = INIT_PID_CONFIG(
      80.0f, 0.1f, 1.0f, 1.5f, 800.0f, 15000.0f,
      (pid_improvement_e)(PID_Trapezoid_Intergral | PID_Integral_Limit |
                          PID_Derivative_On_Measurement),
      (pid_mode_e)PID_POSITION);
  Pid_Config_t motor_pid_init_imu_z = INIT_PID_CONFIG(
      50.0f, 0.5f, 0.2f, 0.5f, 800.0f, 5000.0f,
      (pid_improvement_e)(PID_Trapezoid_Intergral | PID_Integral_Limit |
                          PID_Derivative_On_Measurement),
      (pid_mode_e)PID_POSITION);
  moto_control_pid.pid_speed_wt1000_X =
      PID_struct_init(&motor_pid_init_wt1000_x);
  moto_control_pid.pid_speed_wt1000_Y =
      PID_struct_init(&motor_pid_init_wt1000_y);
  moto_control_pid.pid_speed_imu_Z = PID_struct_init(&motor_pid_init_imu_z);
  // Encoder_Init();
  // START_lianjie();
}
/********************************END OF FILE****************************/
