/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 18:53:23
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\MPU6050\mpu6050.c
 * @Description:
 */
#include "mpu6050.h"
/**
 * 初始化MPU模块
 *
 * 本函数负责初始化MPU模块，包括IIC通信初始化、设置MPU的工作模式、加速度计和陀螺仪的敏感度、采样率，
 * 以及相关的中断和FIFO设置。最后，通过读取设备ID来验证MPU是否正确初始化。
 *
 * @return 返回初始化结果，0表示成功，1表示失败。
 */
u8 MPU_Init(void) {
  u8 res;
  // 初始化IIC通信
  IIC_Init();
  // 复位MPU，置位睡眠模式
  MPU_Write_Byte(MPU_PWR_MGMT1_REG, 0X80);
  // 延时100ms，等待复位完成
  vTaskDelay(100);
  // 清除睡眠模式，使MPU工作正常
  MPU_Write_Byte(MPU_PWR_MGMT1_REG, 0X00);
  // 设置陀螺仪的敏感度为2000dps
  MPU_Set_Gyro_Fsr(3);
  // 设置加速度计的敏感度为2g
  MPU_Set_Accel_Fsr(0);
  // 设置采样率为50Hz
  MPU_Set_Rate(50);
  // 关闭所有中断
  MPU_Write_Byte(MPU_INT_EN_REG, 0X00);
  // 清除用户控制寄存器的配置
  MPU_Write_Byte(MPU_USER_CTRL_REG, 0X00);
  // 关闭FIFO
  MPU_Write_Byte(MPU_FIFO_EN_REG, 0X00);
  // 设置中断生成配置
  MPU_Write_Byte(MPU_INTBP_CFG_REG, 0X80);

  // 读取MPU设备ID，用于验证初始化是否成功
  res = MPU_Read_Byte(MPU_DEVICE_ID_REG);
  if (res == MPU_ADDR) {
    // 配置电源管理寄存器，使MPU工作在正常模式
    MPU_Write_Byte(MPU_PWR_MGMT1_REG, 0X01);
    // 配置电源管理寄存器2，使加速度计工作在正常模式
    MPU_Write_Byte(MPU_PWR_MGMT2_REG, 0X00);
    // 再次设置采样率为50Hz，确保配置生效
    MPU_Set_Rate(50);
  } else {
    // 如果设备ID读取失败，则返回初始化失败
    return 1;
  }
  // 初始化成功，返回0
  return 0;
}

/**
 * 设置陀螺仪的满量程范围(FSR: Full Scale Range)
 *
 * @param fsr
 * 指定的陀螺仪满量程范围，通过左移3位后写入配置寄存器。有效值通常为0~3，对应不同的FSR。
 * @return 返回写入操作的结果状态。成功返回0，失败返回非0值。
 */
u8 MPU_Set_Gyro_Fsr(u8 fsr) {
  // 将陀螺仪的满量程范围设置值左移3位，然后写入陀螺仪配置寄存器
  return MPU_Write_Byte(MPU_GYRO_CFG_REG, fsr << 3);
}

/**
 * 设置MPU的加速度计满量程范围
 *
 * @param fsr
 * 加速度计满量程范围选择，此参数决定加速度计所能测量的加速度的最大值。
 *            可选的范围通过左移3位后作为写入数据传入函数，具体可选范围和左移位数
 *            由MPU的硬件规格决定。
 * @return
 * 返回写入操作的结果状态。如果写入成功，返回写入的值；如果写入失败，则返回错误状态。
 */
u8 MPU_Set_Accel_Fsr(u8 fsr) {
  // 将加速度计的满量程范围设置值左移3位，然后写入MPU的加速度配置寄存器
  return MPU_Write_Byte(MPU_ACCEL_CFG_REG, fsr << 3);
}
/**
 * 设置 MPU 的低通滤波器 (LPF) 频率配置。
 *
 * @param lpf 指定的低通滤波器频率，单位为 Hz。
 * @return 返回 MPU 写字节操作的结果状态码，成功为 0，失败为非 0 值。
 */
u8 MPU_Set_LPF(u16 lpf) {
  u8 data = 0;  // 初始化设置数据

  // 根据传入的 lpf 频率选择合适的配置值
  if (lpf >= 188)
    data = 1;
  else if (lpf >= 98)
    data = 2;
  else if (lpf >= 42)
    data = 3;
  else if (lpf >= 20)
    data = 4;
  else if (lpf >= 10)
    data = 5;
  else
    data = 6;  // 如果 lpf 都不满足上述条件，则默认设置为 6

  // 写入配置数据到 MPU 的配置寄存器
  return MPU_Write_Byte(MPU_CFG_REG, data);
}

/**
 * 设置 MPU 的采样率。
 *
 * @param rate 指定的采样率，单位为 Hz。
 * @return 返回 MPU 设置低通滤波器（LPF）后的实际采样率。
 */
u8 MPU_Set_Rate(u16 rate) {
  u8 data;

  // 限制采样率的范围在 4 Hz 到 1000 Hz 之间
  if (rate > 1000) rate = 1000;
  if (rate < 4) rate = 4;

  // 计算配置寄存器需要的数据值，以设置采样率
  data = 1000 / rate - 1;

  // 写入数据到 MPU 的采样率寄存器
  data = MPU_Write_Byte(MPU_SAMPLE_RATE_REG, data);

  // 设置低通滤波器，并返回调整后的采样率
  return MPU_Set_LPF(rate / 2);
}

/**
 * 获取MPU传感器的温度值
 *
 * 本函数无需传入参数，直接从MPU传感器中读取温度数据，
 * 并将读取的原始数据经过计算转换为更易理解的温度值。
 * 温度值以摄氏度为单位，精确到百分度。
 *
 * @return 返回传感器测量的温度值，单位为摄氏度*100。
 */
short MPU_Get_Temperature(void) {
  u8 buf[2];  // 用于存储从传感器读取的温度数据的临时缓冲区
  short raw;  // 存储原始温度数据
  float temp;  // 存储计算后的温度值

  // 从MPU传感器的温度输出高字节寄存器读取2字节数据
  MPU_Read_Len(MPU_ADDR, MPU_TEMP_OUTH_REG, 2, buf);

  // 将读取的两个字节数据合并为一个16位的原始温度值
  raw = ((u16)buf[0] << 8) | buf[1];

  // 使用预定义的公式将原始温度数据转换为实际温度值
  temp = 36.53 + ((double)raw) / 340;

  // 将温度值乘以100，以便于后续处理或显示
  return temp * 100;
}

/**
 * 从MPU获取陀螺仪数据
 *
 * @param gx 指向存储X轴陀螺仪数据的短整型变量的指针
 * @param gy 指向存储Y轴陀螺仪数据的短整型变量的指针
 * @param gz 指向存储Z轴陀螺仪数据的短整型变量的指针
 * @return 若读取成功返回0，否则返回错误代码
 */
u8 MPU_Get_Gyroscope(short *gx, short *gy, short *gz) {
  u8 buf[6], res;
  // 从MPU的陀螺仪X轴高位寄存器开始读取6个字节的数据
  res = MPU_Read_Len(MPU_ADDR, MPU_GYRO_XOUTH_REG, 6, buf);
  if (res == 0) {
    // 将读取到的數據转换为短整型并存储到gx, gy, gz中
    *gx = ((u16)buf[0] << 8) | buf[1];
    *gy = ((u16)buf[2] << 8) | buf[3];
    *gz = ((u16)buf[4] << 8) | buf[5];
  }
  return res;
  ;
}

/**
 * 获取加速度计数据
 *
 * 本函数用于从MPU设备读取加速度计的三轴数据（X、Y、Z轴）。
 *
 * @param ax 指向存储X轴加速度数据的短整型变量的指针。
 * @param ay 指向存储Y轴加速度数据的短整型变量的指针。
 * @param az 指向存储Z轴加速度数据的短整型变量的指针。
 * @return 返回值为操作结果，成功返回0，失败返回错误码。
 */
u8 MPU_Get_Accelerometer(short *ax, short *ay, short *az) {
  u8 buf[6], res;
  // 从MPU加速度计的寄存器读取6字节数据（X轴高8位、低8位，Y轴同理，Z轴同理）
  res = MPU_Read_Len(MPU_ADDR, MPU_ACCEL_XOUTH_REG, 6, buf);
  if (res == 0) {
    // 数据解析：将读取的字节合并为16位短整型加速度值
    *ax = ((u16)buf[0] << 8) | buf[1];
    *ay = ((u16)buf[2] << 8) | buf[3];
    *az = ((u16)buf[4] << 8) | buf[5];
  }
  return res;
  ;
}

/**
 * @brief 向MPU设备的指定寄存器写入长度为len的数据。
 *
 * 此函数通过I2C与MPU设备通信，向给定寄存器写入指定数量的字节。它处理I2C起始条件、
 * 地址传输、寄存器地址和数据字节，以及等待ACK和停止条件。
 *
 * @param addr MPU设备的地址。
 * @param reg 要向其写入数据的MPU设备中的寄存器。
 * @param len 要写入寄存器的字节数。
 * @param buf 包含要写入数据的缓冲区指针。
 * @return u8 如果数据写入成功返回0，如果在传输过程中发生ACK错误返回1。
 */
u8 MPU_Write_Len(u8 addr, u8 reg, u8 len, u8 *buf) {
  u8 i;

  // 发送I2C起始条件以开始通信
  IIC_Start();
  // 发送设备地址，后跟读/写位（0表示写）
  IIC_Send_Byte((addr << 1) | 0);

  // 检查发送设备地址后是否收到ACK
  if (IIC_Wait_Ack()) {
    // 如果未收到ACK，终止I2C通信
    IIC_Stop();
    return 1;
  }

  // 向MPU设备中的寄存器地址发送数据
  IIC_Send_Byte(reg);
  // 发送寄存器地址后等待ACK
  IIC_Wait_Ack();

  // 遍历缓冲区并发送每个数据字节，每次发送后等待ACK。如果发生ACK错误，终止通信。
  for (i = 0; i < len; i++) {
    IIC_Send_Byte(buf[i]);
    if (IIC_Wait_Ack()) {
      IIC_Stop();
      return 1;
    }
  }

  // 在所有数据成功发送后，终止I2C通信
  IIC_Stop();
  return 0;  // 表示数据写入成功完成
}

/**
 * 从MPU设备指定寄存器读取数据长度
 *
 * @param addr MPU设备地址
 * @param reg 要读取的寄存器地址
 * @param len 要读取的数据长度
 * @param buf 用于存储读取数据的缓冲区指针
 * @return 读取成功返回0，失败返回1
 */
u8 MPU_Read_Len(u8 addr, u8 reg, u8 len, u8 *buf) {
  // 启动I2C通信
  IIC_Start();
  // 发送设备地址和读取指令
  IIC_Send_Byte((addr << 1) | 0);
  // 等待ACK，若失败则终止通信
  if (IIC_Wait_Ack()) {
    IIC_Stop();
    return 1;
  }
  // 发送寄存器地址
  IIC_Send_Byte(reg);
  IIC_Wait_Ack();
  // 重新启动I2C通信，切换为读模式
  IIC_Start();
  IIC_Send_Byte((addr << 1) | 1);
  IIC_Wait_Ack();

  // 循环读取数据
  while (len) {
    // 如果是读取最后一个字节，设置为不等待ACK
    if (len == 1)
      *buf = IIC_Read_Byte(0);
    else
      *buf = IIC_Read_Byte(1);
    len--;
    buf++;
  }

  // 结束I2C通信
  IIC_Stop();
  return 0;
}

/**
 * @brief 向 MPU 写入一个字节数据
 *
 * @param reg 要写入的数据地址
 * @param data 要写入的具体数据
 * @return u8 返回操作是否成功。成功返回0，失败返回1。
 */
u8 MPU_Write_Byte(u8 reg, u8 data) {
  // 开始 IIC 通信
  IIC_Start();
  // 发送 MPU 地址，并设置为写模式
  IIC_Send_Byte((MPU_ADDR << 1) | 0);
  // 等待 ACK，如果未收到 ACK，则结束通信并返回错误
  if (IIC_Wait_Ack()) {
    IIC_Stop();
    return 1;
  }
  // 发送寄存器地址
  IIC_Send_Byte(reg);
  // 等待 ACK
  IIC_Wait_Ack();
  // 发送数据
  IIC_Send_Byte(data);
  // 等待 ACK，如果未收到 ACK，则结束通信并返回错误
  if (IIC_Wait_Ack()) {
    IIC_Stop();
    return 1;
  }
  // 结束 IIC 通信
  IIC_Stop();
  return 0;
}

/**
 * 从MPU设备读取一个字节数据
 * @param reg 要读取的寄存器地址
 * @return 从指定寄存器读取到的数据
 */
u8 MPU_Read_Byte(u8 reg) {
  u8 res;  // 用于存储从MPU寄存器读取的数据

  // 启动I2C通信
  IIC_Start();
  // 发送MPU设备地址和读取指令
  IIC_Send_Byte((MPU_ADDR << 1) | 0);
  IIC_Wait_Ack();  // 等待ACK确认
  // 发送要读取的寄存器地址
  IIC_Send_Byte(reg);
  IIC_Wait_Ack();  // 等待ACK确认

  // 重新启动I2C通信，进行数据读取
  IIC_Start();
  // 发送MPU设备地址和读取指令
  IIC_Send_Byte((MPU_ADDR << 1) | 1);
  IIC_Wait_Ack();  // 等待ACK确认
  // 读取数据
  res = IIC_Read_Byte(0);

  // 结束I2C通信
  IIC_Stop();

  return res;  // 返回读取到的数据
}
