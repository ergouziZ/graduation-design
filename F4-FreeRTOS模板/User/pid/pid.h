/***
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 19:19:07
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\pid\pid.h
 * @Description:
 */

#ifndef __pid_H
#define __pid_H
/* Includes ------------------------------------------------------------------*/
//#include "stm32f4xx_hal.h"
#include "main.h"

#define PID_NUM_MAX 30  // 最大PID实例数

// 使用宏初始化Pid_Config_t
#define INIT_PID_CONFIG(Kp_val, Ki_val, Kd_val, Kf_val, IntegralLimit_val, \
                        MaxOut_val, Improve_val, mode)                     \
  {                                                                        \
    .Kp = Kp_val, .Ki = Ki_val, .Kd = Kd_val, .Kf = Kf_val,                \
    .IntegralLimit = IntegralLimit_val, .MaxOut = MaxOut_val,              \
    .Improve = Improve_val, .pid_mode = mode,                              \
  }

/* PID 优化环节使能标志位,通过位与可以判断启用的优化环节 */
typedef enum {
  PID_IMPROVE_NONE = 0X00,                 // 0000 0000
  PID_Integral_Limit = 0x01,               // 0000 0001 积分限幅
  PID_Derivative_On_Measurement = 0x02,    // 0000 0010 微分先行
  PID_Trapezoid_Intergral = 0x04,          // 0000 0100 梯形积分
  PID_Proportional_On_Measurement = 0x08,  // 0000 1000
  PID_OutputFilter = 0x10,                 // 0001 0000 输出滤波
  PID_ChangingIntegrationRate = 0x20,      // 0010 0000 变速积分
  PID_DerivativeFilter = 0x40,             // 0100 0000 微分滤波器
  PID_ErrorHandle = 0x80,                  // 1000 0000
} pid_improvement_e;

/* PID 报错类型枚举*/
typedef enum error_type_e {
  PID_ERROR_NONE = 0x00U,
  PID_MOTOR_BLOCKED_ERROR = 0x01U
} error_type_e;

typedef struct {
  uint64_t error_count;
  error_type_e error_type;
} pid_ErrorHandler_t;

enum { LLAST = 0, LAST = 1, NOW = 2 };

typedef enum { PID_POSITION = 3, PID_DELTA = 4 } pid_mode_e;
/* 用于PID初始化的结构体*/
typedef struct Pid_Config_  // config parameter
    {
  // basic parameter
  float Kp;
  float Ki;
  float Kd;
  float Kf;
  float MaxOut;    // 输出限幅
  float DeadBand;  // 死区
  pid_improvement_e Improve;
  float IntegralLimit;  // 积分限幅
  float CoefA;  // AB为变速积分参数,变速积分实际上就引入了积分分离
  float CoefB;          // ITerm = Err*((A-abs(err)+B)/A)  when B<|err|<A+B
  float Output_LPF_RC;  // RC = 1/omegac
  float Derivative_LPF_RC;
  pid_mode_e pid_mode;
} Pid_Config_t;

/*pid结构体*/
typedef struct __pid_t {
  float p;
  float i;
  float d;
  float f;

  float set[3];     //目标值,包含NOW， LAST， LLAST上上次
  float get[3];     //测量值，包含NOW， LAST， LLAST上上次
  float err[3];     //误差，包含NOW， LAST， LLAST上上次
  float Output[3];  //输出值，包含NOW， LAST

  float pout;  // p输出
  float iout;  // i输出
  float dout;  // d输出
  float fout;  // f输出

  float ITerm;  //积分

  float pos_out;       //本次位置式输出
  float last_pos_out;  //上次输出
  float delta_u;       //本次增量值
  float delta_out;     //本次增量式输出 = last_delta_out + delta_u
  float last_delta_out;
  float Last_Dout;

  float max_err;
  float deadband;  // err < deadband return
  pid_mode_e pid_mode;

  float MaxOutput;      //输出限幅
  float IntegralLimit;  //积分限幅
  float CoefA;          // 变速积分 For Changing Integral
  float CoefB;  // 变速积分 ITerm = Err*((A-abs(err)+B)/A)  when B<|err|<A+B
  float Output_LPF_RC;      // 输出滤波器 RC = 1/omegac
  float Derivative_LPF_RC;  // 微分滤波器系数

  uint32_t last_dt_cnt;
  float dt;

  pid_improvement_e Improve;
  pid_ErrorHandler_t ERRORHandler;

  void (*f_param_init)(struct __pid_t *pid, Pid_Config_t *config);
  void (*f_pid_reset)(struct __pid_t *pid, float p, float i, float d,
                      float f);  // pid三个参数修改
} pid_t;

typedef struct gimbal_controller_t {
  pid_t *pid_speed_wt1000_X;
  pid_t *pid_speed_wt1000_Y;
  pid_t *pid_speed_imu_Z;
} robot_control_pid_t;

pid_t *PID_struct_init(Pid_Config_t *config);
float pid_calc(pid_t *pid, float fdb, float ref);
void pid_reset(pid_t *pid, float kp, float ki, float kd, float kf);
void abs_max_or_min_limit(int *a, int ABS_MAX);       //*限幅
void abs_max_or_min_limit1(float *a, float ABS_MAX);  //*限幅
void abs_max_or_min_limit2(int *a, float ABS_MAX);    //*限幅
float average_filter(float new_value);
float average_filter1(float new_value);
//定义成全局变量
extern robot_control_pid_t moto_control_pid;
#endif
