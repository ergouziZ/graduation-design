/*
 *                   ___====-_  _-====___
 *             _--^^^#####//      \\#####^^^--_
 *          _-^##########// (    ) \\##########^-_
 *         -############//  |\^^/|  \\############-
 *       _/############//   (@::@)   \############\_
 *      /#############((     \\//     ))#############\
 *     -###############\\    (oo)    //###############-
 *    -#################\\  / VV \  //#################-
 *   -###################\\/      \//###################-
 *  _#/|##########/\######(   /\   )######/\##########|\#_
 *  |/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
 *  `  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
 *     `   `  `      `   / | |  | | \   '      '  '   '
 *                      (  | |  | |  )
 *                     __\ | |  | | /__
 *                    (vvv(VVV)(VVV)vvv)
 *
 *      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *                神兽保佑            永无BUG
 */

/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 20:01:08
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\pid\pid.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\pid\pid.c
 * @Description:
 */
/* Includes ------------------------------------------------------------------*/
#include "./pid./pid.h"

static uint8_t idx = 0;  // register idx,是该文件的全局PID索引,在注册时使用
                         /* PID控制器的实例,此处仅保存指针,内存的分配将通过实例初始化时通过malloc()进行
                          */
static pid_t *pid_obj[PID_NUM_MAX] = {NULL};

//定义pid结构体
robot_control_pid_t moto_control_pid = {NULL};

#define ABS(x) ((x > 0) ? (x) : (-x))

/**
 * 对给定的浮点数进行限幅处理。
 *
 * 该函数接受一个浮点数指针和一个绝对最大值作为参数，将指针所指的浮点数
 * 限制在绝对最大值的范围内。如果浮点数超过绝对最大值，则将其设置为
 * 绝对最大值；如果浮点数低于绝对最大值的相反数，则将其设置为绝对最大值
 * 的相反数。
 *
 * @param a 浮点数指针，指向需要进行限幅处理的浮点数值。
 * @param ABS_MAX 绝对最大值，浮点数将被限制在这个值的范围内。
 *
 * 无返回值，直接通过指针修改原始浮点数。
 */

void abs_limit(float *a, float ABS_MAX)  //*限幅
{
  // 如果数值大于绝对最大值，则将其设置为绝对最大值
  if (*a > ABS_MAX) *a = ABS_MAX;
  // 如果数值小于绝对最大值的相反数，则将其设置为绝对最大值的相反数
  if (*a < -ABS_MAX) *a = -ABS_MAX;
}

/**
 * @brief 对给定的整型变量进行限幅操作
 *
 * 本函数用于将指定整型变量的值限制在绝对最大值(ABS_MAX)的范围内。
 * 如果变量的值超出这个范围，它将被调整到该范围的边界值。
 *
 * @param a 指向需要进行限幅操作的整型变量的指针
 * @param ABS_MAX 允许的绝对最大值，函数将确保变量的值不超过此值
 */
void abs_max_or_min_limit(int *a, int ABS_MAX)  //*限幅
{
  // 如果变量的值大于允许的绝对最大值，则将其调整为绝对最大值
  if (*a > ABS_MAX) *a = ABS_MAX;
  // 如果变量的值小于允许的绝对最小值(-ABS_MAX)，则将其调整为绝对最小值的相反数
  if (*a < -ABS_MAX) *a = -ABS_MAX;
}

// void abs_max_or_min_limit1(float *a, float ABS_MAX)  //*限幅
// {
//   if (*a > ABS_MAX) *a = ABS_MAX;
//   if (*a < -ABS_MAX) *a = -ABS_MAX;
// }
// void abs_max_or_min_limit2(int *a, float ABS_MAX)  //*限幅
// {
//   if (*a > ABS_MAX) *a = ABS_MAX;
//   if (*a < -ABS_MAX) *a = -ABS_MAX;
// }

/**
 * @brief 初始化 PID 控制器参数
 *
 * 该函数用于根据给定的配置信息，初始化一个 PID
 * 控制器对象的参数。配置信息包括积分限制、
 * 最大输出值、PID
 * 模式、死区、改进算法参数、比例增益、积分增益、微分增益和预测增益。
 * 同时，该函数会将该 PID 对象添加到全局对象数组中，并更新数组的索引。
 *
 * @param object 指向 PID 控制器对象的指针。
 * @param config 指向包含 PID 控制器配置信息的结构体的指针。
 */
static void pid_param_init(pid_t *object, Pid_Config_t *config) {
  // 初始化 PID 对象的参数
  object->IntegralLimit = config->IntegralLimit;
  object->MaxOutput = config->MaxOut;
  object->pid_mode = config->pid_mode;
  object->deadband = config->DeadBand;
  object->Improve = config->Improve;
  object->p = config->Kp;
  object->i = config->Ki;
  object->d = config->Kd;
  object->f = config->Kf;

  // 将 PID 对象添加到全局对象数组，并更新索引
  pid_obj[idx++] = object;
}
/*
 * 功能：重置PID控制器的参数
 * 参数：
 * - pid：指向PID结构体的指针，用于修改PID控制器的参数
 * - kp：比例增益参数
 * - ki：积分增益参数
 * - kd：微分增益参数
 * - kf：预测增益参数
 * 返回值：无
 * 注释：该函数用于在运行时更改PID控制器的参数，对PID结构体内的参数进行直接赋值。
 */
void pid_reset(pid_t *pid, float kp, float ki, float kd, float kf) {
  // 将新的PID控制参数赋值给指向的结构体
  pid->p = kp;
  pid->i = ki;
  pid->d = kd;
  pid->f = kf;
}

/* ----------------------------以下是pid优化环节的实现----------------------------
 */

/*
 * 函数名称：f_Trapezoid_Intergral
 * 功能描述：计算梯形法则下的积分值，并更新积分项。
 * 参数列表：
 *    - pid: 指向包含积分器状态的pid结构体的指针。
 * 返回值：无。
 */

static void f_Trapezoid_Intergral(pid_t *pid) {
  // 计算梯形面积，并更新积分项
  // 通过当前误差和上一误差的平均值，以及时间差dt，计算积分值
  pid->ITerm = pid->i * ((pid->err[NOW] + pid->err[LAST]) / 2) * pid->dt;
}

/**
 * 函数名称: f_Changing_Integration_Rate
 * 功能描述: 根据当前误差及累积趋势调整积分项的值。
 * 参数列表:
 *    - pid: 指向pid控制结构体的指针，包含PID控制器的各种参数和状态。
 * 返回值: 无
 */

static void f_Changing_Integration_Rate(pid_t *pid) {
  // 判断积分趋势，若误差和积分输出符号相同，表示积分在累积
  if (pid->err[NOW] * pid->iout > 0) {
    // 判断当前误差是否在全积分范围内
    if (ABS(pid->err[NOW]) <= pid->CoefB)
      return;  // 当前误差绝对值小于等于CoefB时，不做任何调整，保持当前积分值

    // 判断当前误差是否在部分积分范围内
    if (ABS(pid->err[NOW]) <= (pid->CoefA + pid->CoefB))
      // 调整积分项的值，以限制积分的效果
      pid->ITerm *= (pid->CoefA - ABS(pid->err[NOW]) + pid->CoefB) / pid->CoefA;
    else  // 当前误差超出最大阈值，清零积分项
      pid->ITerm = 0;
  }
}

/**
 * @brief 限制PID控制器的积分作用范围。
 *
 * 该函数用于防止PID控制器的积分项超出预定的最大限制，避免因积分饱和导致的控制系统不稳定。
 * 同时，当输出达到最大值或最小值时，根据当前误差与积分项的符号决定是否清零积分项，以避免积分作用持续增强。
 *
 * @param pid
 * 指向包含PID控制参数的结构体的指针。该结构体应包含成员：MaxOutput（最大输出值）、IntegralLimit（积分限制）、iout（当前积分项值）、ITerm（内部积分项值）、err（误差数组）。
 * 注意：该函数不检查参数结构体的合法性，调用前应确保其已被正确初始化。
 *
 * @note 该函数为静态函数，即其作用范围仅限于当前源文件。
 */
static void f_Integral_Limit(pid_t *pid) {
  // 检查传入的PID结构体指针是否为NULL，防止解引用空指针
  if (pid == NULL) {
    return;
  }

  // 检查PID控制参数是否合理，MaxOutput和IntegralLimit应为正数
  if (pid->MaxOutput <= 0 || pid->IntegralLimit <= 0) {
    // 在这里可以添加日志记录或异常处理代码
    return;
  }

  static float temp_Output, temp_Iout;
  temp_Iout = pid->iout + pid->ITerm;

  // 检查输出是否超出最大值，并根据误差与积分项符号决定是否清零积分项
  if (ABS(temp_Output) > pid->MaxOutput) {
    // 如果积分方向与当前误差方向一致，则清零积分项，以避免积分作用持续增强
    if (pid->err[NOW] * pid->iout > 0) {
      pid->ITerm = 0;
    }
  }

  // 检查积分项是否超出限制范围，并进行调整
  if (temp_Iout > pid->IntegralLimit) {
    // 积分项超出上限，将其限制在上限值，并调整当前积分项值
    pid->ITerm = 0;
    pid->iout = pid->IntegralLimit;
  } else if (temp_Iout <
             -pid->IntegralLimit) {  // 检查积分项是否低于下限，并进行调整
    // 积分项低于下限，将其限制在下限值，并调整当前积分项值
    pid->ITerm = 0;
    pid->iout = -pid->IntegralLimit;
  }
}

/**
 * 计算PID控制器的微分输出。
 *
 * 该函数用于根据当前和上一时刻的测量值计算PID控制器的微分输出。
 * 微分项有助于减少系统的超调和提高响应速度。
 *
 * @param pid 指向PID控制器结构体的指针，包含PID的配置和状态信息。
 */
static void f_Derivative_On_Measurement(pid_t *pid) {
  // 计算微分输出
  pid->dout = pid->d * (pid->get[LAST] - pid->get[NOW]) / pid->dt;
}

/**
 * 函数名称: f_Derivative_Filter
 * 功能描述: 对PID控制器的微分项进行滤波处理。
 * 参数列表:
 *    - pid: 指向包含PID控制相关参数的结构体的指针。
 * 返回值: 无
 */

static void f_Derivative_Filter(pid_t *pid) {
  // 计算微分输出，采用了一种低通滤波器（LPF）的处理方式来减少微分项的噪声。
  pid->dout = pid->dout * pid->dt / (pid->Derivative_LPF_RC + pid->dt) +
              pid->Last_Dout * pid->Derivative_LPF_RC /
                  (pid->Derivative_LPF_RC + pid->dt);
}

/**
 * 函数名称: f_Output_Filter
 * 功能描述: 对PID输出进行低通滤波处理。
 * 参数列表:
 *    - pid: 指向包含PID控制相关参数和状态的结构体的指针。
 * 返回值: 无
 */

static void f_Output_Filter(pid_t *pid) {
  // 根据低通滤波器的公式更新当前输出值
  pid->Output[NOW] =
      pid->Output[NOW] * pid->dt / (pid->Output_LPF_RC + pid->dt) +
      pid->Output[LAST] * pid->Output_LPF_RC / (pid->Output_LPF_RC + pid->dt);
}

/**
 * @brief PID错误处理函数
 * 该函数用于处理PID控制过程中可能出现的电机阻塞错误。通过对PID输出和设定值的判断，
 * 来确定电机是否处于阻塞状态，并对阻塞情况进行计数，当阻塞次数超过一定阈值时，
 * 认为电机存在严重阻塞问题，并记录相应的错误类型。
 *
 * @param pid 指向PID控制结构体的指针，包含PID的控制参数及状态信息。
 */
static void f_PID_ErrorHandle(pid_t *pid) {
  /* 判断是否满足认为电机阻塞的条件 */
  if (fabsf(pid->Output[NOW]) < pid->MaxOutput * 0.001f ||
      fabsf(pid->set[NOW]) < 0.0001f)
    return;  // 如果当前PID输出很小或设定值接近于0，则不认为是阻塞

  /* 计算当前设定值与最大输出值之差占设定值的比例，判断电机是否接近完全阻塞 */
  if ((fabsf(pid->set[NOW] - pid->MaxOutput) / fabsf(pid->set[NOW])) > 0.95f) {
    // 如果比例大于0.95，认为电机已阻塞，进行计数
    pid->ERRORHandler.error_count++;
  } else {
    // 如果比例不大于0.95，认为电机未阻塞，重置计数器
    pid->ERRORHandler.error_count = 0;
  }

  /* 当阻塞错误计数超过阈值时，设置错误类型为电机阻塞错误 */
  if (pid->ERRORHandler.error_count > 500) {
    // 当阻塞次数超过500次，认为电机存在严重阻塞问题
    pid->ERRORHandler.error_type = PID_MOTOR_BLOCKED_ERROR;
  }
}

/**
 * 函数：dwt_get_delta
 * 功能：计算并返回两个时间戳之间的差值（delta
 * time），使用了循环定时器的滴答作为时间单位。
 * 参数：
 *   -
 * cnt_last：指向一个uint32_t类型变量的指针，该变量存储了上一次调用此函数时的滴答计数。
 * 返回值：
 *   - 返回从上一次调用此函数到当前调用之间的时间差，单位为秒。
 */
static float dwt_get_delta(uint32_t *cnt_last) {
  // 获取当前的滴答计数
  volatile uint32_t cnt_now = xTaskGetTickCount();
  // 计算时间差，并转换为秒
  float dt = ((uint32_t)(cnt_now - *cnt_last)) / ((float)(configTICK_RATE_HZ));
  // 更新上一次的滴答计数为当前计数
  *cnt_last = cnt_now;
  return dt;
}

/* ---------------------------下面是PID的外部算法接口---------------------------*/

/**
 * PID 计算函数
 *
 * 对给定的反馈值和参考值进行PID计算，并返回PID输出值。
 * 这个函数支持位置式和增量式PID控制，并且可以应用一系列的改进策略，如梯形积分、变速积分、微分先行等。
 *
 * @param pid 指向PID结构体的指针，包含PID的参数和状态。
 * @param fdb 反馈值。
 * @param ref 参考值或目标值。
 * @return 计算得到的PID输出值。
 */
float pid_calc(pid_t *pid, float fdb, float ref) {
  if (NULL == pid) {
    printf("pid_calc: pid is NULL\n");
    return 0;
  }
  // 堵转检测
  if (pid->Improve & PID_ErrorHandle) f_PID_ErrorHandle(pid);

  // 更新时间戳
  pid->dt = dwt_get_delta(&pid->last_dt_cnt);
  pid->get[NOW] = fdb;
  pid->set[NOW] = ref;
  pid->err[NOW] = ref - fdb;  // 计算误差

  // 检查误差是否超过最大值或处于死区
  if (pid->max_err != 0 && ABS(pid->err[NOW]) > pid->max_err) return 0;
  if (pid->deadband != 0 && ABS(pid->err[NOW]) < pid->deadband) return 0;

  // 根据PID模式进行计算
  if (pid->pid_mode == PID_POSITION)  // 位置式PID控制
  {
    // 计算比例、积分、微分和预测项
    pid->pout = pid->p * pid->err[NOW];
    pid->ITerm = pid->i * pid->err[NOW] * pid->dt;
    pid->iout += pid->ITerm;
    pid->dout = pid->d * (pid->err[NOW] - pid->err[LAST]) / pid->dt;
    pid->fout = pid->f * (pid->set[NOW] - pid->set[LAST]);

    // 应用改进策略
    if (pid->Improve & PID_Trapezoid_Intergral) f_Trapezoid_Intergral(pid);
    if (pid->Improve & PID_ChangingIntegrationRate)
      f_Changing_Integration_Rate(pid);
    if (pid->Improve & PID_Derivative_On_Measurement)
      f_Derivative_On_Measurement(pid);
    if (pid->Improve & PID_DerivativeFilter) f_Derivative_Filter(pid);
    if (pid->Improve & PID_Integral_Limit) f_Integral_Limit(pid);

    // 应用输出滤波
    if (pid->Improve & PID_OutputFilter) f_Output_Filter(pid);

    // 计算最终输出
    pid->pos_out = pid->pout + pid->iout + pid->dout + pid->fout;

    // 输出限幅
    abs_limit(&(pid->pos_out), pid->MaxOutput);
    pid->last_pos_out = pid->pos_out;     // 更新上一次的输出值
  } else if (pid->pid_mode == PID_DELTA)  // 增量式PID控制
  {
    // 在增量式中重复计算，除了输出计算和更新部分
    // 计算比例、积分、微分和预测项
    pid->pout = pid->p * pid->err[NOW];
    pid->ITerm = pid->i * pid->err[NOW] * pid->dt;
    pid->iout += pid->ITerm;
    pid->dout = pid->d * (pid->err[NOW] - pid->err[LAST]) / pid->dt;
    pid->fout = pid->f * (pid->set[NOW] - pid->set[LAST]);

    // 应用改进策略
    if (pid->Improve & PID_Trapezoid_Intergral) f_Trapezoid_Intergral(pid);
    if (pid->Improve & PID_ChangingIntegrationRate)
      f_Changing_Integration_Rate(pid);
    if (pid->Improve & PID_Derivative_On_Measurement)
      f_Derivative_On_Measurement(pid);
    if (pid->Improve & PID_DerivativeFilter) f_Derivative_Filter(pid);
    if (pid->Improve & PID_Integral_Limit) f_Integral_Limit(pid);

    // 应用输出滤波
    if (pid->Improve & PID_OutputFilter) f_Output_Filter(pid);

    // 计算增量输出并更新总输出
    pid->delta_u = pid->pout + pid->iout + pid->dout;
    pid->delta_out = pid->last_delta_out + pid->delta_u;

    // 输出限幅
    abs_limit(&(pid->delta_out), pid->MaxOutput);
    pid->last_delta_out = pid->delta_out;  // 更新上一次的输出值
  }

  // 更新状态变量
  pid->err[LLAST] = pid->err[LAST];
  pid->err[LAST] = pid->err[NOW];
  pid->get[LLAST] = pid->get[LAST];
  pid->get[LAST] = pid->get[NOW];
  pid->set[LLAST] = pid->set[LAST];
  pid->set[LAST] = pid->set[NOW];
  pid->Last_Dout = pid->dout;

  // 根据PID模式返回最终输出
  (pid->pid_mode == PID_POSITION) ? (pid->Output[NOW] = pid->pos_out)
                                  : (pid->Output[NOW] = pid->delta_out);
  return pid->Output[NOW];  // 返回输出值
  //
}

/**
 * @brief 清除PID控制器的状态。
 *
 * 该函数用于将PID控制器的各项参数重置为初始状态，以便于重新开始控制过程。
 *
 * @param pid 指向PID控制器结构体的指针。该结构体包含了PID控制所需的所有变量。
 */
void pid_clear(pid_t *pid) {
  // 清除PID的获取值和误差值
  pid->get[NOW] = 0;
  pid->get[LAST] = 0;
  pid->err[NOW] = 0;
  pid->err[LAST] = 0;

  // 清除PID的输出值和积分项
  pid->pout = 0;   // 比例输出
  pid->iout = 0;   // 积分输出
  pid->dout = 0;   // 微分输出
  pid->ITerm = 0;  // 积分项的当前值

  // 清除输出值和上一周期的微分输出
  pid->Output[NOW] = 0;
  pid->Output[LAST] = 0;
  pid->Last_Dout = 0;  // 上一周期的微分输出

  // 重置错误处理机制的状态
  pid->ERRORHandler.error_count = 0;              // 错误计数器重置
  pid->ERRORHandler.error_type = PID_ERROR_NONE;  // 重置错误类型为无错误
}

/**
 * 初始化 PID 控制器对象
 *
 * 本函数负责为 PID
 * 控制器分配内存，并初始化其参数。它需要一个配置结构体作为参数，
 * 并返回一个指向 PID 对象的指针。若内存分配失败或函数指针初始化不成功，则返回
 * NULL。
 *
 * @param config 指向 Pid_Config_t 结构体的指针，包含 PID 的配置参数。
 * @return 返回一个指向初始化好的 PID 对象的指针。若失败，则返回 NULL。
 */
pid_t *PID_struct_init(Pid_Config_t *config) {
  if (NULL == config) {
    printf("PID_Config: config is NULL.\n");
    return NULL;
  }
  // 分配内存给 PID 对象
  pid_t *object = (pid_t *)malloc(sizeof(pid_t));
  if (object == NULL) {
    // 内存分配失败，记录错误或返回错误码
    perror("PID_struct_init: Memory allocation failed");
    return NULL;
  }

  // 初始化函数指针前进行非空验证
  if (pid_param_init == NULL || pid_reset == NULL) {
    free(object);  // 资源清理
    fprintf(stderr,
            "PID_struct_init: Function pointer initialization failed.\n");
    return NULL;
  }

  // 初始化函数指针
  object->f_param_init = pid_param_init;
  object->f_pid_reset = pid_reset;

  // 使用配置参数初始化 PID 对象
  object->f_param_init(object, config);

  return object;
}

#define AVERAGE_N 5

/**
 * average_filter - 计算并返回一个新值与过去N个值的平均值的平均值
 * @new_value: 要加入平均计算的新值
 *
 * 本函数使用一个静态缓冲区来存储过去N个值（由AVERAGE_N定义），
 * 并计算新值与这些值的平均值的平均值。这在处理如传感器数据平滑等场景时非常有用。
 * 注意：AVERAGE_N必须至少为1。
 *
 * 返回: 新值与过去N个值的平均值的平均值（浮点数）
 */
float average_filter(float new_value) {
  static float average_value_buf[AVERAGE_N];  // 存储过去N个值的静态缓冲区
  static float average_sum = 0.0;  // 过去N个值的和，初始化为0
  uint16_t count;                  // 使用更大的计数器类型

  // 检查new_value是否为NaN或无穷大
  if (!isfinite(new_value)) {
    // 这里选择忽略异常值，返回当前的平均值
    return average_sum / AVERAGE_N;
  }

  // 从旧的平均值中减去第一个值（即将被替换的值）
  average_sum -= average_value_buf[0];

  // 将所有值向前移动一位，为新值腾出位置
  for (count = 0; count < AVERAGE_N - 1; count++) {
    average_value_buf[count] = average_value_buf[count + 1];
  }

  // 将新值加入到缓冲区的最后
  average_value_buf[AVERAGE_N - 1] = new_value;

  // 更新过去N个值的和
  average_sum += new_value;

  // 计算并返回平均值
  return average_sum / AVERAGE_N;
}
float average_filter1(float new_value) {
  static float average_value_buf[AVERAGE_N];
  static float average_sum = 0;
  uint8_t count;
  average_sum -= average_value_buf[0];
  for (count = 0; count < AVERAGE_N - 1; count++) {
    average_value_buf[count] = average_value_buf[count + 1];
  }
  average_value_buf[AVERAGE_N - 1] = new_value;
  average_sum += average_value_buf[AVERAGE_N - 1];

  return (average_sum / (AVERAGE_N * 1.0));
}
