/***
 * @Author: your name
 * @Date: 2024-05-12 22:06:11
 * @LastEditTime: 2024-05-12 22:59:19
 * @LastEditors: your name
 * @Description:
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart3\usart3.h
 * @可以输入预定的版权声明、个性签名、空行等
 */
#ifndef usart3_H
#define usart3_H

#include "main.h"
/***********   USART3   *********/
#define USART3_EN 1         // 使能（1）/禁止（0）串口3使用
#define USART3_DMA_TX_EN 0  // 使能（1）/禁止（0）串口DMA发送
#define USART3_DMA_RX_EN 1  // 使能（1）/禁止（0）串口DMA接收

extern u8 Rx_Buff[Rx_BUF_SIZE];       // 接收数据缓冲区
extern u8 TEXT_TO_SEND[Rx_BUF_SIZE];  // 发送数据缓冲区
#define Data_Length 300
extern u16 USART33_RX_BUF[Rx_BUF_SIZE];  // 双缓存接收数据缓冲区
extern float slam_date;
extern uint8_t Zigbee_data[13];
extern u16 Time_Reset;

void USART3_Init(u32 bound);
void usart3_dma_rx(void);
void usart3_rx(void);

void MYDMA_Enable(DMA_Stream_TypeDef *DMA_Streamx, u16 ndtr);
#endif
