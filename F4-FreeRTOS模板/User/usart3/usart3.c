/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:11
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 19:54:15
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart3\usart3.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart3\usart3.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart3\usart3.c
 * @Description:
 */

#include "./usart3./usart3.h"

/***********   USART3   *********/
u16 USART33_RX_BUF[Rx_BUF_SIZE];  // 接收
u8 Rx_Buff[Rx_BUF_SIZE];          // 发送数据缓冲区
u8 TEXT_TO_SEND[Rx_BUF_SIZE];     // 发送缓存
uint8_t Zigbee_data[13];
u16 Time_Reset;
float slam_date = 0;

/**
 * @brief 初始化USART3
 * @param bound 串口波特率
 * 该函数初始化USART3，包括GPIO端口设置、USART配置、NVIC设置以及DMA配置（如果使能）。
 */
void USART3_Init(u32 bound) {
  // GPIO端口初始化结构体
  GPIO_InitTypeDef GPIO_InitStructure;
  // USART初始化结构体
  USART_InitTypeDef USART_InitStructure;
  // NVIC初始化结构体
  NVIC_InitTypeDef NVIC_InitStructure;

  // 启用GPIOB和USART3的时钟
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

  // 配置USART3的TX和RX引脚为复用功能
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3);

  // 配置TX引脚
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  // 配置RX引脚
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  // 配置USART3参数并初始化
  USART_InitStructure.USART_BaudRate = bound;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl =
      USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART3, &USART_InitStructure);

// 配置中断（根据是否使能DMA选择不同的中断）
#if USART3_DMA_RX_EN
  USART_ITConfig(USART3, USART_IT_IDLE, ENABLE);  // 使用空闲中断进行DMA接收
#else
  USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);  // 使用接收中断
#endif

  // 启用USART3
  USART_Cmd(USART3, ENABLE);

  // 配置USART3的NVIC
  NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

// 如果使能DMA接收，配置DMA并启用USART3的DMA接收
#if USART3_DMA_RX_EN
  MYDMA_Config(DMA1_Stream1, DMA_Channel_4, (u32)&USART3->DR, (u32)Rx_Buff,
               Data_Length, (u32)0x00000000);
  USART_DMACmd(USART3, USART_DMAReq_Rx, ENABLE);
  DMA_Cmd(DMA1_Stream1, ENABLE);
#endif
// 如果使能DMA传输，配置DMA
#if USART3_DMA_TX_EN
  MYDMA_Config(DMA1_Stream3, DMA_Channel_4, (u32)&USART3->DR, (u32)TEXT_TO_SEND,
               Data_Length, (u32)0x00000040);
#endif
}

/**
 * @brief 处理USART3的中断请求。
 *
 * 该函数根据USART3是否启用了DMA接收来选择相应的数据接收处理流程。
 * 如果启用了USART3的DMA接收，则调用usart3_dma_rx()函数处理数据接收；
 * 否则，调用usart3_rx()函数进行处理。
 *
 * @note 函数没有参数和返回值，因为它是作为中断服务例程使用的。
 */
void USART3_IRQHandler(void) {
#if USART3_DMA_RX_EN
  // 如果启用了USART3的DMA接收，则调用DMA方式的接收处理函数
  usart3_dma_rx();
#else
  // 如果未启用DMA接收，则调用常规方式的接收处理函数
  usart3_rx();
#endif
}
// 开启一次DMA传输
// DMA_Streamx:DMA数据流,DMA1_Stream0~7/DMA2_Stream0~7
// ndtr:数据传输量
#if USART3_DMA_TX_EN
void MYDMA_Enable(DMA_Stream_TypeDef *DMA_Streamx, u16 ndtr) {
  DMA_Cmd(DMA_Streamx, DISABLE);                  // 关闭DMA传输
  USART_DMACmd(USART3, USART_DMAReq_Tx, ENABLE);  // 使能串口1的DMA发送
  while (DMA_GetCmdStatus(DMA_Streamx) != DISABLE)
    ;                                         // 确保DMA可以被设置
  DMA_SetCurrDataCounter(DMA_Streamx, ndtr);  // 数据传输量
  DMA_Cmd(DMA_Streamx, ENABLE);               // 开启DMA传输  启动传输
  //  printf("标志位=%d", DMA_GetFlagStatus(DMA2_Stream7, DMA_FLAG_TCIF7));
  while (DMA_GetFlagStatus(DMA_Streamx, DMA_FLAG_TCIF3) != SET)
    ;  // 等待DMA2_Steam7传输完成  传输完成时标志位为1
  DMA_Cmd(DMA_Streamx, DISABLE);               // 关闭DMA传输
  DMA_ClearFlag(DMA_Streamx, DMA_FLAG_TCIF3);  // 清除DMA2_Steam7传输完成标志
}
#endif

#if USART3_DMA_RX_EN
/**
 * @brief 使用DMA处理USART3的接收功能。
 * 该函数检查USART3是否完成了数据接收，如果完成，则处理接收到的数据。
 * 数据处理包括验证数据格式和长度，并将有效数据发送到一个队列中。
 *
 */
void usart3_dma_rx(void) {
  // 检查USART3是否处于空闲状态，即数据传输完成
  if (USART_GetITStatus(USART3, USART_IT_IDLE) != RESET) {
    uint32_t len;
    remote_data_t data;
    // 清除USART的空闲状态标志
    USART3->SR;
    USART3->DR;
    // 关闭DMA接收
    DMA_Cmd(DMA1_Stream1, DISABLE);

    // 计算实际接收到的数据长度
    len = Data_Length - DMA_GetCurrDataCounter(DMA1_Stream1);

    // 如果接收到的数据长度为13字节，进行进一步的校验和处理
    if (len == 13) {
      // 数据格式校验
      if (Rx_Buff[0] == 0xA6 && Rx_Buff[1] == 0x6A && Rx_Buff[12] == 0xAA) {
        Time_Reset = 0;
        // 从接收缓冲区提取有效数据并发送到队列
        memcpy(&data, &Rx_Buff[2], sizeof(uint8_t) * 10);
        xQueueSendFromISR(Remoter_Queue, &data, NULL);
      }
    }
    // 清除DMA传输完成的标志位
    DMA_ClearFlag(DMA1_Stream1, DMA_FLAG_TCIF1);
    // 设置DMA接收缓冲区的长度
    DMA_SetCurrDataCounter(DMA1_Stream1, Data_Length);
    // 重新启用DMA接收
    DMA_Cmd(DMA1_Stream1, ENABLE);
  }
}
#else
void usart3_rx(void) {
  static u8 seri_count = 0;
  u16 check_sum = 0;
  static u8 i;
  static u8 flag;

  if (USART_GetITStatus(USART3, USART_IT_RXNE) != RESET) {
    //    i=USART_ReceiveData(USART3);

    LED1 = !LED1;
    if (USART_ReceiveData(USART3) == 0xb3) {
      flag = 1;
    }

    if (flag) {
      USART33_RX_BUF[seri_count++] = USART_ReceiveData(USART3);
      if (seri_count == USART2_Len) {
        if (USART33_RX_BUF[0] == 0xb3 && USART33_RX_BUF[1] == 0xb3) {
          // for (i = 0; i < USART2_Len; i++)
          // {
          // 	check_sum += USART33_RX_BUF[i];
          // 	printf(" \r\n  USART33_RX_BUF[%d]=%x ",i,USART33_RX_BUF[i]);
          // }
          // 			if (USA2_Rx[0] == 0xb3 && USA2_Rx[1] == 0xb3)
          // {
          param.Get_radius = (USART33_RX_BUF[8] << 8) + USART33_RX_BUF[7];
          return_value[0] = (USART33_RX_BUF[3] << 8) + USART33_RX_BUF[2];
          return_value[1] = (USART33_RX_BUF[5] << 8) + USART33_RX_BUF[4];
          param.Get_shape = USART33_RX_BUF[6];
          // }

          statu->OP_Data_status = READY;
          printf("\r\n                                     (%d,%d)",
                 return_value[0], return_value[1]);

          seri_count = 0;
          flag = 0;
          // statu->Data_status = READY;
          // printf("\r\n  %d",xTaskGetTickCount());
        }
      }
    }
    USART_ClearFlag(USART3, USART_FLAG_RXNE);
  }
}
#endif
