/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 19:37:47
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\pwm\pwm.c
 * @Description:
 */

#include "./pwm./pwm.h"

void TIM14_PWM_Init(u32 arr, u32 psc)  // X Y舵机
{
  // 定义结构体指针
  GPIO_InitTypeDef GPIO_InitStructure;
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
  TIM_OCInitTypeDef TIM_OCInitStructure;

  // 使能时钟
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);  // 使能定时器14的时钟
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);  // 使能GPIOA时钟

  // 初始化GPIOF9 F10为复用推挽输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  // GPIO复用映射到定时器14
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_TIM14);

  // 初始化定时器14
  TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInitStruct.TIM_Period = arr;
  TIM_TimeBaseInitStruct.TIM_Prescaler = psc;
  TIM_TimeBaseInit(TIM14, &TIM_TimeBaseInitStruct);

  // 初始化输出比较参数
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_Pulse = 1300;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  TIM_OC1Init(TIM14, &TIM_OCInitStructure);

  // 使能预装载寄存器
  TIM_OC1PreloadConfig(TIM14, TIM_OCPreload_Enable);
  TIM_ARRPreloadConfig(TIM14, ENABLE);

  // 使能定时器
  TIM_Cmd(TIM14, ENABLE);
}

void TIM3_PWM_Init(u32 arr, u32 psc)  // X舵机
{
  // 定义结构体指针
  GPIO_InitTypeDef GPIO_InitStructure;
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
  TIM_OCInitTypeDef TIM_OCInitStructure;

  // 使能时钟
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);  // 使能定时器3的时钟
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);  // 使能GPIOA时钟

  // 初始化GPIOF9 F10为复用推挽输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  // GPIO复用映射到定时器14
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_TIM3);

  // 初始化定时器14
  TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInitStruct.TIM_Period = arr;
  TIM_TimeBaseInitStruct.TIM_Prescaler = psc;
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStruct);

  // 初始化输出比较参数
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_Pulse = 1200;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  TIM_OC2Init(TIM3, &TIM_OCInitStructure);

  // 使能预装载寄存器
  TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
  TIM_ARRPreloadConfig(TIM3, ENABLE);

  // 使能定时器
  TIM_Cmd(TIM3, ENABLE);
}
/**
 * @brief        :
 * @param         {u32} arr:
 * @param         {u32} psc:
 * @return        {*}
 **/
void TIM8_PWM_Init(u32 arr, u32 psc)  // X Y舵机
{
  // 定义结构体指针
  GPIO_InitTypeDef GPIO_InitStructure;
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
  TIM_OCInitTypeDef TIM_OCInitStructure;
  //	TIM_OCInitTypeDef TIM_OCInitStructure;

  // 使能时钟
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);  // 使能定时器8的时钟
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);  // 使能GPIOA时钟

  // 初始化GPIOF9 F10为复用推挽输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Pin =
      GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  // GPIO复用映射到定时器8
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_TIM8);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_TIM8);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_TIM8);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_TIM8);

  // 初始化定时器14
  TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;  // 分频 只对输入有效
  TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInitStruct.TIM_Period = arr;
  TIM_TimeBaseInitStruct.TIM_Prescaler = psc;
  TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM8, &TIM_TimeBaseInitStruct);

  /* ================== 输出结构体初始化 =================== */
  // 配置为 PWM 模式 1，先输出高电平，达到比较值的时候再改变电平
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  // 主输出使能
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  // 互补输出使能
  TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
  // 配置比较值
  TIM_OCInitStructure.TIM_Pulse = 0;
  // 主输出高电平有效
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  // 互补输出高电平有效
  TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
  // 主输出在被禁止时为高电平
  TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
  // 互补输出在被禁止时为低电平
  TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;

  // 初始化输出比较参数
  TIM_OC1Init(TIM8, &TIM_OCInitStructure);
  TIM_OC2Init(TIM8, &TIM_OCInitStructure);
  TIM_OC3Init(TIM8, &TIM_OCInitStructure);
  TIM_OC4Init(TIM8, &TIM_OCInitStructure);

  // 使能预装载寄存器
  TIM_OC1PreloadConfig(TIM8, TIM_OCPreload_Enable);
  TIM_OC2PreloadConfig(TIM8, TIM_OCPreload_Enable);
  TIM_OC3PreloadConfig(TIM8, TIM_OCPreload_Enable);
  TIM_OC4PreloadConfig(TIM8, TIM_OCPreload_Enable);

  TIM_ARRPreloadConfig(TIM8, ENABLE);

  TIM_CtrlPWMOutputs(TIM8, ENABLE);  // 高级定时器pwm输出使能
  // 使能定时器
  TIM_Cmd(TIM8, ENABLE);
}
