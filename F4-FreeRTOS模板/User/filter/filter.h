#ifndef __FILTER_H
#define __FILTER_H
#include "main.h"

//#define LIMIT_FILTER   						1
//#define MIDDLE_FILTER 					    2
//#define AVERAGE_FILTER 						3
//#define FIRST_ORDER_LAG_FILTER 				4
//#define WEIGHTED_FILTER 					5

enum{
		LIMIT_FILTER,   						
		MIDDLE_FILTER, 					    
		AVERAGE_FILTER,						
		FIRST_ORDER_LAG_FILTER, 				
		WEIGHTED_FILTER, 					
};

typedef struct __filter_t
{
	uint32_t DEVIATION;
	float new_value;
	float value;
	uint32_t mode;
	
	void (*f_param_init)(struct __filter_t *filter,  //PID参数初始化
                    uint32_t mode,
                    uint32_t DEVIATION);
    void (*f_filter_reset)(struct __filter_t *filter,uint32_t DEVIATION);		//pid三个参数修改
}filter_t;

void FILTER_struct_init(filter_t *filter,
			uint32_t mode,uint32_t 	DEVIATION);//滤波算法参数初始化
float filter_math(filter_t* filter,float new_value);//滤波算法计算
 void filter_reset(filter_t *filter,uint32_t DEVIATION);
float shake( float new_value , float now_value);

#endif


