/*
 *                                                     __----~~~~~~~~~~~------___
 *                                    .  .   ~~//====......          __--~ ~~
 *                    -.            \_|//     |||\\  ~~~~~~::::... /~
 *                 ___-==_       _-~o~  \/    |||  \\            _/~~-
 *         __---~~~.==~||\=_    -_--~/_-~|-   |\\   \\        _/~
 *     _-~~     .=~    |  \\-_    '-~7  /-   /  ||    \      /
 *   .~       .~       |   \\ -_    /  /-   /   ||      \   /
 *  /  ____  /         |     \\ ~-_/  /|- _/   .||       \ /
 *  |~~    ~~|--~~~~--_ \     ~==-/   | \~--===~~        .\
 *           '         ~-|      /|    |-~\~~       __--~~
 *                       |-~~-_/ |    |   ~\_   _-~            /\
 *                            /  \     \__   \/~                \__
 *                        _--~ _/ | .-~~____--~-/                  ~~==.
 *                       ((->/~   '.|||' -_|    ~~-/ ,              . _||
 *                                  -_     ~\      ~~---l__i__i__i--~~_/
 *                                  _-~-__   ~)  \--______________--~~
 *                                //.-~~~-~_--~- |-------~~~~~~~~
 *                                       //.-~~~--\
 *                       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *                               神兽保佑            永无BUG
 */
/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 18:38:00
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\control\control.c
 * @Description:
 */
#include "./control./control.h"

/*X轴PID参数*/
float kp_x = 0.1;
float ki_x = 0.0;
float kd_x = 1.0;

float kp_x1 = 0.1;
float ki_x1 = 0.0;
float kd_x1 = 1.0;
/*Y轴PID参数*/
float kp_y = 0.0;
float ki_y = 0.0;
float kd_y = 0.0;

float kp_y1 = 0.0;
float ki_y1 = 0.0;
float kd_y1 = 0.0;

/*实际值*/
int place_x = 0;
int place_y = 0;
int place_z = 0;

/*5个位置的坐标保存在二维数组*/
int set_place[7][3] = {100, 500, 0,     // 位置1
                       100, 500, -90,   // 位置2
                       400, 500, -90,   // 位置3
                       400, 500, -180,  // 位置4
                       400, 100, -180,  // 位置5
                       400, 100, 90,    //位置6
                       100, 100, 90};

static int last_x;

// 四个电机
motor_controller_t motor[4];
// 摇控的数据
remote_data_t remote_data;
/**
 * @brief 初始化电机控制器
 *
 * 该函数对四个电机进行初始化，设置它们的速度为0，方向为0，并配置每个电机的GPIO端口。
 * 每个电机有两个GPIO引脚用于控制。
 */
void motor_init(void) {
  // 初始化电机0的配置
  motor[0].speed = 0;                            // 设置初始速度为0
  motor[0].direction = 0;                        // 设置初始方向为0
  motor[0].motor_set_dir = motor_set_direction;  // 设置方向改变的函数
  // 配置电机0的两个GPIO引脚
  motor[0].moto_io[0].moto_port = LED1_GPIO_PORT;  // 设置第一个GPIO端口
  motor[0].moto_io[0].moto_pin = LED1_PIN;         // 设置第一个GPIO引脚
  motor[0].moto_io[1].moto_port = LED2_GPIO_PORT;  // 设置第二个GPIO端口
  motor[0].moto_io[1].moto_pin = LED2_PIN;         // 设置第二个GPIO引脚

  // 初始化电机1的配置（配置过程与电机0相似，略去详细注释）
  motor[1].speed = 0;
  motor[1].direction = 0;
  motor[1].motor_set_dir = motor_set_direction;
  motor[1].moto_io[0].moto_port = LED3_GPIO_PORT;
  motor[1].moto_io[0].moto_pin = LED3_PIN;
  motor[1].moto_io[1].moto_port = LED4_GPIO_PORT;
  motor[1].moto_io[1].moto_pin = LED4_PIN;

  // 初始化电机2的配置
  motor[2].speed = 0;
  motor[2].direction = 0;
  motor[2].motor_set_dir = motor_set_direction;
  motor[2].moto_io[0].moto_port = LED5_GPIO_PORT;
  motor[2].moto_io[0].moto_pin = LED5_PIN;
  motor[2].moto_io[1].moto_port = LED6_GPIO_PORT;
  motor[2].moto_io[1].moto_pin = LED6_PIN;

  // 初始化电机3的配置
  motor[3].speed = 0;
  motor[3].direction = 0;
  motor[3].motor_set_dir = motor_set_direction;
  motor[3].moto_io[0].moto_port = LED7_GPIO_PORT;
  motor[3].moto_io[0].moto_pin = LED7_PIN;
  motor[3].moto_io[1].moto_port = LED8_GPIO_PORT;
  motor[3].moto_io[1].moto_pin = LED8_PIN;
}

/**
 * 设置电机转动方向
 *
 * 根据电机的速度来决定电机的转动方向，并通过GPIO口对电机的控制引脚进行高低电平设置，从而改变电机的转动方向。
 *
 * @param motor
 * 指向MOTOR结构体的指针，MOTOR结构体中包含了电机的相关信息，如速度、方向和控制GPIO口的信息。
 */

void motor_set_direction(motor_controller_t *motor) {
  // 当电机速度大于0时，设置电机正转
  if (motor->speed > 0) {
    motor->direction = 1;  // 设置方向为正转
    GPIO_SetBits(motor->moto_io[0].moto_port,
                 motor->moto_io[0].moto_pin);  // 控制引脚1设置为高电平
    GPIO_ResetBits(motor->moto_io[1].moto_port,
                   motor->moto_io[1].moto_pin);  // 控制引脚2设置为低电平
    // 当电机速度小于0时，设置电机反转
  } else if (motor->speed < 0) {
    motor->direction = 2;  // 设置方向为反转
    GPIO_ResetBits(motor->moto_io[0].moto_port,
                   motor->moto_io[0].moto_pin);  // 控制引脚1设置为低电平
    GPIO_SetBits(motor->moto_io[1].moto_port,
                 motor->moto_io[1].moto_pin);  // 控制引脚2设置为高电平
    // 当电机速度为0时，停止电机转动
  } else if (motor->speed == 0) {
    motor->direction = 0;  // 设置方向为停止
    GPIO_ResetBits(motor->moto_io[0].moto_port,
                   motor->moto_io[0].moto_pin);  // 控制引脚1和2都设置为低电平
    GPIO_ResetBits(motor->moto_io[1].moto_port,
                   motor->moto_io[1].moto_pin);  // 控制引脚2也设置为低电平
  }
}

/**
 * @brief 设置所有电机的方向
 *
 * 该函数遍历电机数组，调用每个电机的motor_set_dir函数来设置其方向。
 * 无需参数。
 * 无返回值。
 */
static void moto_direction(void) {
  // 计算电机数组的元素数量
  uint8_t count = 0;
  count = (sizeof(motor) / sizeof(motor[0]));

  // 遍历电机数组，设置每个电机的方向
  for (size_t i = 0; i < count; i++) {
    motor[i].motor_set_dir(&motor[i]);
  }
}

/**
 * @brief 控制电机运行的函数。
 * 该函数根据当前模式（遥控或巡航），结合传感器数据，计算并调整电机的速度和方向，
 * 以实现对机器人的控制。
 */
void moto_control(void) {
  // 巡航模式控制逻辑
  if (mode == 1) {
    //从队列中读取各个传感器的数据
    BaseType_t queuerevcev_flag[4] = {0};
    tof_data_t tof_data;
    imu_data_t imu_data;
    tw1000_data_t tw1000_data;
    oled_data_t oled_data;
    CT_data_t ct_data;

    queuerevcev_flag[0] = xQueueReceive(Tof_Data_Queue, &tof_data, 0);
    queuerevcev_flag[1] = xQueueReceive(Imu_Data_Queue, &imu_data, 0);
    queuerevcev_flag[2] = xQueueReceive(Tw1000_Data_Queue, &tw1000_data, 0);

    //屏幕显示数据传递
    if (queuerevcev_flag[0] && queuerevcev_flag[1] && queuerevcev_flag[2]) {
      memcpy(&oled_data.tof_data, &tof_data, sizeof(tof_data));
      memcpy(&oled_data.imu_data, &imu_data, sizeof(imu_data));
      memcpy(&oled_data.tw1000_data, &tw1000_data, sizeof(tw1000_data));
      xQueueSend(Oled_Data_Queue, &oled_data, NULL);
    }
    // 无球状态下，根据目标位置计算电机速度
    if (0 == tof_data.red_flag && queuerevcev_flag[1] && queuerevcev_flag[2]) {
      memcpy(&ct_data.imu_data, &imu_data, sizeof(imu_data));
      memcpy(&ct_data.tw1000_data, &tw1000_data, sizeof(tw1000_data));
      xQueueOverwrite(CT_Queue, &ct_data);
      // 对三个轴（X、Y、Z）进行PID计算，得出各个轴的速度
      pid_calc(moto_control_pid.pid_speed_wt1000_X, tw1000_data.x, place_x);
      pid_calc(moto_control_pid.pid_speed_wt1000_Y, tw1000_data.y, place_y);
      pid_calc(moto_control_pid.pid_speed_imu_Z, imu_data.Z, place_z);

      // 当Z轴角度偏差过大时，调整输出速度方向
      if (abs(imu_data.Z - place_z) > 130)
        moto_control_pid.pid_speed_imu_Z->Output[NOW] =
            -moto_control_pid.pid_speed_imu_Z->Output[NOW];

      // 根据目标Z轴角度，调整电机旋转方向
      switch (place_z) {
        case 0:
          // 0度时，电机按默认方向旋转
          moto_control_pid.pid_speed_wt1000_X->Output[NOW] =
              moto_control_pid.pid_speed_wt1000_X->Output[NOW];
          moto_control_pid.pid_speed_wt1000_Y->Output[NOW] =
              moto_control_pid.pid_speed_wt1000_Y->Output[NOW];
          break;
        case -90:
          // -90度时，电机方向反转
          // last_x = moto_speed[1].pos_out;
          // moto_speed[1].pos_out = -moto_speed[0].pos_out;
          // moto_speed[0].pos_out = last_x;

          last_x = moto_control_pid.pid_speed_wt1000_Y->Output[NOW];
          moto_control_pid.pid_speed_wt1000_Y->Output[NOW] =
              -moto_control_pid.pid_speed_wt1000_X->Output[NOW];
          moto_control_pid.pid_speed_wt1000_X->Output[NOW] = last_x;

        case -180:
          // -180度时，电机方向反转
          // last_x = -moto_speed[1].pos_out;
          // moto_speed[1].pos_out = -moto_speed[1].pos_out;
          // moto_speed[0].pos_out = -moto_speed[0].pos_out;

          moto_control_pid.pid_speed_wt1000_X->Output[NOW] =
              -moto_control_pid.pid_speed_wt1000_X->Output[NOW];
          moto_control_pid.pid_speed_wt1000_Y->Output[NOW] =
              -moto_control_pid.pid_speed_wt1000_Y->Output[NOW];

          break;
        case 90:
          // 90度时，电机方向反转
          // last_x = moto_speed[1].pos_out;
          // moto_speed[1].pos_out = -moto_speed[0].pos_out;
          // moto_speed[0].pos_out = last_x;

          last_x = moto_control_pid.pid_speed_wt1000_Y->Output[NOW];
          moto_control_pid.pid_speed_wt1000_Y->Output[NOW] =
              -moto_control_pid.pid_speed_wt1000_X->Output[NOW];
          moto_control_pid.pid_speed_wt1000_X->Output[NOW] = last_x;

          break;
        default:
          // 其他角度不调整方向
          break;
      }

      // 计算并调整四个电机的速度输出
      motor[0].speed = moto_control_pid.pid_speed_wt1000_X->Output[NOW] +
                       moto_control_pid.pid_speed_wt1000_Y->Output[NOW] -
                       moto_control_pid.pid_speed_imu_Z->Output[NOW];
      motor[1].speed = -moto_control_pid.pid_speed_wt1000_X->Output[NOW] +
                       moto_control_pid.pid_speed_wt1000_Y->Output[NOW] -
                       moto_control_pid.pid_speed_imu_Z->Output[NOW];
      motor[2].speed = -moto_control_pid.pid_speed_wt1000_X->Output[NOW] +
                       moto_control_pid.pid_speed_wt1000_Y->Output[NOW] +
                       moto_control_pid.pid_speed_imu_Z->Output[NOW];
      motor[3].speed = moto_control_pid.pid_speed_wt1000_X->Output[NOW] +
                       moto_control_pid.pid_speed_wt1000_Y->Output[NOW] +
                       moto_control_pid.pid_speed_imu_Z->Output[NOW];

      // 对电机速度进行最大最小限制
      abs_max_or_min_limit(&motor[0].speed, 15000);
      abs_max_or_min_limit(&motor[1].speed, 15000);
      abs_max_or_min_limit(&motor[2].speed, 15000);
      abs_max_or_min_limit(&motor[3].speed, 15000);
    }
    // 找到小球后，限制最大速度，调整电机速度
    else if (1 == tof_data.red_flag && queuerevcev_flag[0] &&
             queuerevcev_flag[2]) {
      // pid_calc(&moto_speed[0], tof_data.X_data, 95);
      // pid_calc(&moto_speed[1], tof_data.Y_data, 71);
      // pid_calc(&moto_speed[2], imu_data.Z, place_z);

      pid_calc(moto_control_pid.pid_speed_wt1000_X, tw1000_data.x, 95);
      pid_calc(moto_control_pid.pid_speed_wt1000_Y, tw1000_data.y, 71);
      pid_calc(moto_control_pid.pid_speed_imu_Z, imu_data.Z, place_z);

      moto_control_pid.pid_speed_wt1000_Y->Output[NOW] =
          -moto_control_pid.pid_speed_wt1000_Y->Output[NOW];

      motor[0].speed = moto_control_pid.pid_speed_wt1000_X->Output[NOW] +
                       moto_control_pid.pid_speed_wt1000_Y->Output[NOW] -
                       moto_control_pid.pid_speed_imu_Z->Output[NOW];
      motor[1].speed = -moto_control_pid.pid_speed_wt1000_X->Output[NOW] +
                       moto_control_pid.pid_speed_wt1000_Y->Output[NOW] -
                       moto_control_pid.pid_speed_imu_Z->Output[NOW];
      motor[2].speed = -moto_control_pid.pid_speed_wt1000_X->Output[NOW] +
                       moto_control_pid.pid_speed_wt1000_Y->Output[NOW] +
                       moto_control_pid.pid_speed_imu_Z->Output[NOW];
      motor[3].speed = moto_control_pid.pid_speed_wt1000_X->Output[NOW] +
                       moto_control_pid.pid_speed_wt1000_Y->Output[NOW] +
                       moto_control_pid.pid_speed_imu_Z->Output[NOW];

      // 对电机速度进行最大最小限制
      abs_max_or_min_limit(&motor[0].speed, 5000);
      abs_max_or_min_limit(&motor[1].speed, 5000);
      abs_max_or_min_limit(&motor[2].speed, 5000);
      abs_max_or_min_limit(&motor[3].speed, 5000);
    }
  }
  // 遥控模式下的电机控制逻辑
  else if (mode == 2) {
    // 处理遥控器发送的数据
    data_processing(&remote_data);
    remote_reset();  //断联保护
    // 根据遥控器输入计算电机速度
    motor[0].speed =
        (remote_data.L_x + remote_data.L_y + remote_data.R_y) * 150;
    motor[1].speed =
        (-remote_data.L_x + remote_data.L_y + remote_data.R_y) * 150;
    motor[2].speed =
        (-remote_data.L_x + remote_data.L_y - remote_data.R_y) * 150;
    motor[3].speed =
        (remote_data.L_x + remote_data.L_y - remote_data.R_y) * 150;

    // 对电机速度进行最大最小限制
    abs_max_or_min_limit(&motor[0].speed, 15000);
    abs_max_or_min_limit(&motor[1].speed, 15000);
    abs_max_or_min_limit(&motor[2].speed, 15000);
    abs_max_or_min_limit(&motor[3].speed, 15000);
  } else {
    // 其他模式下，电机停止
    motor[0].speed = 0;
    motor[1].speed = 0;
    motor[2].speed = 0;
    motor[3].speed = 0;
  }

  // 设置电机方向
  moto_direction();

  // 将计算得到的速度值设置到定时器比较寄存器中，控制电机转动
  TIM_SetCompare1(TIM8, abs(motor[0].speed));
  TIM_SetCompare2(TIM8, abs(motor[1].speed));
  TIM_SetCompare3(TIM8, abs(motor[2].speed));
  TIM_SetCompare4(TIM8, abs(motor[3].speed));
}

/*******************************************************************************************
 *@ 函数名称：void data_processing(void)
 *@ 功能： 数据处理
 *@ 备注： 处理从zigbee遥控接收到的数据
 ********************************************************************************************/

void data_processing(remote_data_t *remote_data) {
  remote_info_t remote_info = {0};
  if (pdPASS == xQueueReceive(Remoter_Queue, &remote_info, 0)) {
    if (remote_info.L_x <= 135 || remote_info.L_x >= 145) {
      remote_data->L_x = (remote_info.L_x - 128);
    } else {
      remote_data->L_x = 0;
    }
    if (remote_info.L_y <= 100 || remote_info.L_y >= 110) {
      remote_data->L_y = (remote_info.L_y - 127);
    } else {
      remote_data->L_y = 0;
    }
    if (remote_info.R_x <= 120 || remote_info.R_x >= 130) {
      remote_data->R_x = (remote_info.R_x - 125);
    } else {
      remote_data->R_x = 0;
    }
    if (remote_info.R_y <= 125 || remote_info.R_y >= 135) {
      remote_data->R_y = (remote_info.R_y - 129);
    } else {
      remote_data->R_y = 0;
    }
    remote_data->P_L = remote_info.P_L;
    remote_data->P_R = remote_info.P_R;
    remote_data->mode = remote_info.mode;
    for (int i = 0; i < 8; i++) {
      remote_data->button[i] = (remote_info.button >> i) & 0x01;
    }
  }
}

/**
 * @brief 实现遥控断电保护功能的函数。
 * 该函数会检查时间重置标志，如果达到预设的阈值（50），则会触发重置编号操作，
 * 以此实现遥控断电的保护机制。
 */

void remote_reset(void) {
  /********************遥控断电保护****************************/

  // 检查时间重置标志是否达到阈值，触发重置编号操作实现遥控断电保护
  if (Time_Reset >= 50) {
    Reset_num();
  }
}

/**
 * @brief 重置数值函数
 * 该函数用于在特定条件下重置一系列的变量值，包括时间重置计数器、坐标值以及Zigbee和蓝牙数据。
 * 该函数不接受参数，也不返回任何值。
 */
void Reset_num(void) {
  // 如果时间重置计数器大于等于10000，则将其重置为100
  if (Time_Reset >= 10000) Time_Reset = 100;

  // 重置坐标值为0
  remote_data.L_x = 0;
  remote_data.L_y = 0;
  remote_data.R_x = 0;
  remote_data.R_y = 0;
  remote_data.P_L = 0;
  remote_data.P_R = 0;
  // 重置蓝牙数据的某一位
  remote_data.button[2] = 0;
}
