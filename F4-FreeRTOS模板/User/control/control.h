/***
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 18:38:59
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\control\control.h
 * @Description:
 */
#ifndef __control_H
#define __control_H
#include "main.h"

typedef struct moto_io {
  GPIO_TypeDef *moto_port;
  uint16_t moto_pin;
} moto_io_t;

typedef struct motor_controller motor_controller_t;
struct motor_controller {
  int speed;
  int direction : 2;
  struct moto_io moto_io[2];
  void (*motor_set_dir)(motor_controller_t *motor);
};

extern float kp_x;
extern float ki_x;
extern float kd_x;

extern float kp_x1;
extern float ki_x1;
extern float kd_x1;

extern float kp_y;
extern float ki_y;
extern float kd_y;

extern float kp_y1;
extern float ki_y1;
extern float kd_y1;

extern float moto_angle_x, moto_angle_y;
extern int place_x;
extern int place_y;
extern int place_z;
extern int set_place[7][3];
extern float qiu_x[10], qiu_y[10];
extern int set_x;
extern int set_y;

extern int PWM_X;
extern int PWM_Y;
extern int PWM_Y_DATE;
extern int PWM_X_DATE;

extern float speed_x;
extern float speed_y;
void moto_control(void);
void Reset_num(void);
void data_processing(remote_data_t *remote_data);
void remote_reset(void);
void motor_init(void);
void motor_set_direction(motor_controller_t *motor);
#endif
