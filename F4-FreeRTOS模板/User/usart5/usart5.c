/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:11
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 20:00:09
 * @FilePath: \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart5\usart5.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart5\usart5.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart5\usart5.c
 * @Description:
 */
#include "./usart5./usart5.h"

// 串口发送缓存区
__align(8) u8
    USART5_TX_BUF[USART5_MAX_SEND_LEN];  // 发送缓冲,最大UART5_MAX_SEND_LEN字节

#ifdef USART5_RX_EN  // 如果使能了接收
// 串口接收缓存区
u8 USART5_RX_BUF
    [USART5_MAX_RECV_LEN];  // 接收缓冲,最大UART5_MAX_RECV_LEN个字节.
u16 USART5_bluetooth_date[21] = {0};
u8 USART5_tx[8] = {0};
u16 USART5_RX_STA = 0;  // 接收数据状态
#endif

const unsigned char auchCRCHi[] = /* CRC??????????????±???*/
    {0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
     0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
     0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
     0x00, 0xC1, 0x81, 0x40};

const unsigned char auchCRCLo[] = /* CRC??????????????±???*/
    {0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7,
     0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E,
     0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9,
     0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
     0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
     0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32,
     0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D,
     0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
     0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF,
     0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
     0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1,
     0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
     0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB,
     0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA,
     0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
     0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
     0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97,
     0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E,
     0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89,
     0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
     0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83,
     0x41, 0x81, 0x80, 0x40};

/******************************************************************************
                                                                                                        CRC???é
*******************************************************************************/
unsigned int CRC_Calculate(unsigned char *pdata, unsigned char num) {
  unsigned char uchCRCHi = 0xFF;
  unsigned char uchCRCLo = 0xFF;
  unsigned char uIndex;
  while (num--) {
    uIndex = uchCRCHi ^ *pdata++;
    uchCRCHi = uchCRCLo ^ auchCRCHi[uIndex];
    uchCRCLo = auchCRCLo[uIndex];
  }
  return (uchCRCHi << 8 | uchCRCLo);
}

// 初始化IO 串口5
// bound:波特率
void USART5_init(u32 bound) {
  NVIC_InitTypeDef NVIC_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure;

  USART_DeInit(UART5);                                   // 复位串口3
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);  // 使能GPIOC时钟
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);  // 使能GPIOC时钟
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);  // 使能UART5时钟

  GPIO_PinAFConfig(GPIOC, GPIO_PinSource12,
                   GPIO_AF_UART5);  // GPIOC12复用为UART5
  GPIO_PinAFConfig(GPIOD, GPIO_PinSource2, GPIO_AF_UART5);  // GPIOD2复用为UART5

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;  // GPIOC12(Tx)和GPIOD2(RX)初始化
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;       // 复用功能
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;  // 速度50MHz
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;     // 推挽复用输出
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;       // 上拉
  GPIO_Init(GPIOC, &GPIO_InitStructure);  // 初始化GPIOC12，和GPIOD2

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;  // GPIOC12(Tx)和GPIOD2(RX)初始化
  GPIO_Init(GPIOD, &GPIO_InitStructure);

  USART_InitStructure.USART_BaudRate = bound;  // 波特率一般设置为9600;
  USART_InitStructure.USART_WordLength =
      USART_WordLength_8b;  // 字长为8位数据格式
  USART_InitStructure.USART_StopBits = USART_StopBits_1;  // 一个停止位
  USART_InitStructure.USART_Parity = USART_Parity_No;     // 无奇偶校验位
  USART_InitStructure.USART_HardwareFlowControl =
      USART_HardwareFlowControl_None;  // 无硬件数据流控制
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;  // 收发模式
  USART_Init(UART5, &USART_InitStructure);  // 初始化串口5

  USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);  // 开启中断

  USART_Cmd(UART5, ENABLE);  // 使能串口

  NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;  // 抢占优先级2
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;         // 子优先级3
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;            // IRQ通道使能
  NVIC_Init(&NVIC_InitStructure);  // 根据指定的参数初始化VIC寄存器
  USART5_RX_STA = 0;               // 清零
}

/**
 * @brief UART5的中断处理程序。
 * 这个函数处理UART5接收到数据的中断请求。
 * 
 * @param None 该函数没有参数。
 * @return None 该函数没有返回值。
 */
void UART5_IRQHandler(void) {
  u8 res;
  // 检查UART5是否接收到新数据
  if (USART_GetITStatus(UART5, USART_IT_RXNE))  
  {
    // 从UART5接收数据
    res = USART_ReceiveData(UART5);
    // 准备处理接收到的数据
    USART5_Receive_Prepare(res);
  }
}

/**
 * 准备接收USART5的数据
 * @param data 接收到的单个字节数据
 * 该函数负责处理USART5接收到的数据，根据帧头和数据长度对数据进行缓存，
 * 并在接收完成时调用处理函数处理接收到的数据。
 */
void USART5_Receive_Prepare(u8 data) {
  /* 局部静态变量：用于缓存接收到的数据 */
  static u8 RxBuffer[21];
  /* 记录接收到的数据长度 */
  static u8 _data_cnt = 0;
  /* 接收状态机的状态 */
  static u8 state = 0;

  /* 等待帧头1 */
  if (state == 0 && data == 0x01) {
    state = 1;
    _data_cnt = 0;
    RxBuffer[_data_cnt++] = data;
  }
  /* 确认帧头2 */
  else if (state == 1 && data == 0x83) {
    state = 2;
    RxBuffer[_data_cnt++] = data;
  }
  /* 接收数据，直到接收完所有数据或遇到错误 */
  else if (state == 2) {
    RxBuffer[_data_cnt++] = data;
    /* 当接收的数据长度达到预期时，处理数据并重置状态 */
    if (_data_cnt == 25) {
      state = 0;
      USART5_access_Data(RxBuffer, _data_cnt);
    }
  }
  /* 如果检测到错误，重置状态，等待新的帧头 */
  else {
    state = 0;
  }
}

/**
 * @brief 将接收到的数据通过USART5传入的数据存储并处理
 *
 * @param data 接收的数据数组
 * @param i 数据数组的长度
 * 存储接收到的数据到全局变量USART5_bluetooth_date中，
 * 并将这些数据传递给get_WT100_measure函数进行处理，
 * 处理后的信息通过队列发送给其他任务。
 */
void USART5_access_Data(u8 data[], u8 i) {
  u8 j = 0;
  tw1000_data_t infifo;

  // 将接收到的数据存储到全局变量中
  for (j = 0; j < i; j++) {
    USART5_bluetooth_date[j] = (u8)data[j];
  }

  // 处理接收到的数据
  get_WT100_measure(&infifo, USART5_bluetooth_date);

  // 将处理后的数据发送到队列中
  xQueueSendFromISR(&Tw1000_Data_Queue, &infifo, NULL);

  // 标记USART5接收完成
  USART5_RX_STA = 1;
}

void USART5_RX_deal(void) {
  if (USART5_RX_STA) {
    USART5_RX_STA = 0;
  }
}

void USART5_send_char(u8 send_value) {
  while (USART_GetFlagStatus(UART5, USART_FLAG_TXE) != SET)  // 等待发送结束
    USART_SendData(UART5, send_value);
  while (USART_GetFlagStatus(UART5, USART_FLAG_TC) != SET)
    ;  // 等待发送结束
}

void Usart5_SendString(uint8_t *data, unsigned int num)  //????3·???×?·???
{
  unsigned int i;
  for (i = 0; i < num; i++) {
    USART5_send_char(data[i]);
  }
}

/**
 * @brief 从缓冲区中获取WT100传感器的测量数据
 * 
 * 该函数负责解析传入的缓冲区，从中提取出WT100传感器的各项测量数据，包括XYZ轴的加速度值、
 * XYZ轴的角度值以及霍尔效应数据，并将这些数据存储到指定的tw1000_data_t结构体中。
 * 
 * @param ptr 指向tw1000_data_t结构体的指针，用于存储从缓冲区解析出的传感器数据。
 * @param buf 包含传感器原始数据的缓冲区，数据顺序特定，函数从中读取数据。
 */
void get_WT100_measure(tw1000_data_t *ptr, uint16_t *buf) {
  // 解析缓冲区中的数据，计算XYZ轴的加速度
  ptr->x = buf[9] << 8 | buf[10];
  ptr->y = buf[11] << 8 | buf[12];
  ptr->z = buf[13] << 8 | buf[14];

  // 解析缓冲区中的数据，计算XYZ轴的角度值，并转换为弧度
  ptr->x_angle = (float)(buf[15] << 8 | buf[16]) / 180.0f;
  ptr->y_angle = (float)(buf[17] << 8 | buf[18]) / 180.0f;
  ptr->z_angle = (float)(buf[19] << 8 | buf[20]) / 180.0f;

  // 解析缓冲区中的数据，得到霍尔效应传感器的值，并转换为无单位的比例值
  ptr->hall = (buf[21] << 8 | buf[22]) / 100.0f;

  // 调整角度值，确保其在-180度到180度之间
  if (ptr->x_angle > 180) ptr->x_angle = ptr->x_angle - 360;
  if (ptr->y_angle > 180) ptr->y_angle = ptr->y_angle - 360;
  if (ptr->z_angle > 180) ptr->z_angle = ptr->z_angle - 360;
}
