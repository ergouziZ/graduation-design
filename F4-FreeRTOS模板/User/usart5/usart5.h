#ifndef __UART5_H
#define __UART5_H
#include "main.h"

#define USART5_MAX_RECV_LEN 400  //�����ջ����ֽ���
#define USART5_MAX_SEND_LEN 400  //����ͻ����ֽ���
#define USART5_RX_EN 1           // 0,������;1,����.

extern u8 USART5_RX_BUF[USART5_MAX_RECV_LEN];  //���ջ���,���UART5_MAX_RECV_LEN�ֽ�
extern u8 USART5_TX_BUF[USART5_MAX_SEND_LEN];  //���ͻ���,���UART5_MAX_SEND_LEN�ֽ�
extern u16 USART5_RX_STA;              //��������״̬
extern u16 USART5_bluetooth_date[21];  //��ȡ����
extern u8 USART5_tx[8];

void USART5_RX_deal(void);
void USART5_access_Data(u8 data[], u8 i);  //����������������
void USART5_Receive_Prepare(u8 data);
void USART5_init(u32 bound);  //����5��ʼ��
// void UART5_printf(char* fmt, ...);
void USART5_send_char(u8 send_value);
void USART5_send_data(u16 value1, u16 value2);
unsigned int CRC_Calculate(unsigned char *pdata, unsigned char num);
void START_positioning(void);
void START_lianjie(void);
void START_STOP(void);
void START_READ(void);
void get_WT100_measure(tw1000_data_t *ptr, uint16_t *buf);
// void UART5_Osc_send_array_float(float array);
#endif
