/*
 * @File Description:
 * @Version: 2.0
 * @Autor: deng
 * @Date: 2022
 * @LastEditors: deng
 * @LastEditTime: 2022
 */
#ifndef _main_h
#define _main_h

/* FreeRTOS */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
// #include "timers.h"
// #include "semphr.h"
// #include "event_groups.h"
// #include "croutine.h"
// #include "portmacro.h"
// #include "portable.h"

/*c语言标准函数库*/
#include "stdbool.h"
#include <string.h>
#include "math.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
/*用户文件*/

typedef struct imu_data_ imu_data_t;
typedef struct tw1000_data_ tw1000_data_t;
typedef struct tof_data_ tof_data_t;
typedef struct remote_info_ remote_info_t;
typedef struct remote_data_ remote_data_t;

#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"
#include "dmpKey.h"
#include "dmpmap.h"
#include "mpu6050.h"

#include "./tesk./user_tesk.h"
#include "./control./control.h"
#include "./pid./pid.h"
#include "./filter./filter.h"

/* 芯片外设 */
#include "bsp_led.h"
#include "bsp_debug_usart.h"
#include "bsp_key.h"
#include "./usart2./usart2.h"
#include "./usart3./usart3.h"
#include "./usart4./usart4.h"
#include "./usart5./usart5.h"
#include "./adc./adc.h"
#include "myiic.h"
#include "./encoder./encoder.h"
#include "./pwm./pwm.h"
#include "./time./time.h"
#include "./OLED./oled.h"

/*************硬件初始化位选***************/
#define USART2_EN 1
#define USART3_EN 1
#define USART4_EN 1
#define ADC_EN 1

// // 前置声明

struct tof_data_ {
  float X_data;
  float Y_data;
  int red_flag;
};

struct imu_data_ {
  float X;
  float Y;
  float Z;
};

struct tw1000_data_ {
  int16_t x;
  int16_t y;
  int16_t z;
  float x_angle;
  float y_angle;  // abs angle range:[0,8191]
  float z_angle;  // abs angle range:[0,8191]
  float hall;
};

struct remote_info_ {
  uint8_t L_x;
  uint8_t L_y;
  uint8_t R_x;
  uint8_t R_y;
  uint8_t P_L;
  uint8_t P_R;
  uint8_t button;
  uint8_t roll;
  uint8_t pitch;
  uint8_t mode;
};

struct remote_data_ {
  int L_x;
  int L_y;
  int R_x;
  int R_y;
  int P_L;
  int P_R;
  bool button[8];
  int roll;
  int pitch;
  uint8_t mode;
};

typedef struct oled_data_ {
  imu_data_t imu_data;
  tw1000_data_t tw1000_data;
  tof_data_t tof_data;
} oled_data_t;

typedef struct CT_data_ {
  imu_data_t imu_data;
  tw1000_data_t tw1000_data;
} CT_data_t;

/******************句柄声明**********************/

/* 任务句柄 */
extern TaskHandle_t Test_Task_Handle; /* LED_tesk */
extern TaskHandle_t KEY_Task_Handle;  /* KEY */
extern TaskHandle_t Test_date_Handle; /* LED */

/* 队列句柄 */
extern QueueHandle_t Tof_Data_Queue;
extern QueueHandle_t Imu_Data_Queue;
extern QueueHandle_t Tw1000_Data_Queue;
extern QueueHandle_t Oled_Data_Queue;
extern QueueHandle_t CT_Queue;
extern QueueHandle_t Remoter_Queue;
/*****************用户任务定义**********************/

void KEY_Task(void *parameter);
void Test_Task(void *parameter);
void Test_date(void *parameter);

#define ready 1
#define no_ready 0

/*****************IO操作**********************/
#define BITBAND(addr, bitnum) \
  ((addr & 0xF0000000) + 0x2000000 + ((addr & 0xFFFFF) << 5) + (bitnum << 2))
#define MEM_ADDR(addr) *((volatile unsigned long *)(addr))
#define BIT_ADDR(addr, bitnum) MEM_ADDR(BITBAND(addr, bitnum))

#define GPIOA_ODR_Addr (GPIOA_BASE + 20)  // 0x40020014
#define GPIOB_ODR_Addr (GPIOB_BASE + 20)  // 0x40020414
#define GPIOC_ODR_Addr (GPIOC_BASE + 20)  // 0x40020814
#define GPIOD_ODR_Addr (GPIOD_BASE + 20)  // 0x40020C14
#define GPIOE_ODR_Addr (GPIOE_BASE + 20)  // 0x40021014
#define GPIOF_ODR_Addr (GPIOF_BASE + 20)  // 0x40021414
#define GPIOG_ODR_Addr (GPIOG_BASE + 20)  // 0x40021814
#define GPIOH_ODR_Addr (GPIOH_BASE + 20)  // 0x40021C14
#define GPIOI_ODR_Addr (GPIOI_BASE + 20)  // 0x40022014

#define GPIOA_IDR_Addr (GPIOA_BASE + 16)  // 0x40020010
#define GPIOB_IDR_Addr (GPIOB_BASE + 16)  // 0x40020410
#define GPIOC_IDR_Addr (GPIOC_BASE + 16)  // 0x40020810
#define GPIOD_IDR_Addr (GPIOD_BASE + 16)  // 0x40020C10
#define GPIOE_IDR_Addr (GPIOE_BASE + 16)  // 0x40021010
#define GPIOF_IDR_Addr (GPIOF_BASE + 16)  // 0x40021410
#define GPIOG_IDR_Addr (GPIOG_BASE + 16)  // 0x40021810
#define GPIOH_IDR_Addr (GPIOH_BASE + 16)  // 0x40021C10
#define GPIOI_IDR_Addr (GPIOI_BASE + 16)  // 0x40022010

#define PAout(n) BIT_ADDR(GPIOA_ODR_Addr, n)
#define PAin(n) BIT_ADDR(GPIOA_IDR_Addr, n)

#define PBout(n) BIT_ADDR(GPIOB_ODR_Addr, n)
#define PBin(n) BIT_ADDR(GPIOB_IDR_Addr, n)

#define PCout(n) BIT_ADDR(GPIOC_ODR_Addr, n)
#define PCin(n) BIT_ADDR(GPIOC_IDR_Addr, n)

#define PDout(n) BIT_ADDR(GPIOD_ODR_Addr, n)
#define PDin(n) BIT_ADDR(GPIOD_IDR_Addr, n)

#define PEout(n) BIT_ADDR(GPIOE_ODR_Addr, n)
#define PEin(n) BIT_ADDR(GPIOE_IDR_Addr, n)

#define PFout(n) BIT_ADDR(GPIOF_ODR_Addr, n)
#define PFin(n) BIT_ADDR(GPIOF_IDR_Addr, n)

#define PGout(n) BIT_ADDR(GPIOG_ODR_Addr, n)
#define PGin(n) BIT_ADDR(GPIOG_IDR_Addr, n)

#define PHout(n) BIT_ADDR(GPIOH_ODR_Addr, n)
#define PHin(n) BIT_ADDR(GPIOH_IDR_Addr, n)

#define PIout(n) BIT_ADDR(GPIOI_ODR_Addr, n)
#define PIin(n) BIT_ADDR(GPIOI_IDR_Addr, n)

#endif
