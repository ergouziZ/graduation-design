/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:11
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 19:56:38
 * @FilePath: \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart4\usart4.c
 * @Description: 
 */

#include "./usart4./usart4.h"

/***********   UART4   *********/
imu_data_t Acc;   // 加速度
imu_data_t Gyro;  // 角速度
imu_data_t Mag;   // 磁场

uint8_t USART4_RX_BUF[Rx_BUF_SIZE];      // 接收
uint8_t UAT4_Rx_Buff[Rx_BUF_SIZE];       // 发送数据缓冲区
uint8_t UAT4_TEXT_TO_SEND[Rx_BUF_SIZE];  // 发送缓存
uint8_t JY_901;

//  JY901B
void UART4_Init(uint32_t bound) {
  // GPIO端口设置
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);  // 使能GPIOA时钟
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);  // 使能USART1时钟

  // 串口1对应引脚复用映射
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource10,
                   GPIO_AF_UART4);  // GPIOA9复用为USART1
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource11,
                   GPIO_AF_UART4);  // GPIOA10复用为USART1

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;    // 复用功能
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;  // 推挽复用输出
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;  // 推挽复用输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;    // 复用功能
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  // USART2相关初始化
  USART_InitStructure.USART_BaudRate = bound;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl =
      USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(UART4, &USART_InitStructure);

#if UART4_DMA_RX_EN
  USART_ITConfig(UART4, USART_IT_IDLE, ENABLE);  // 中断使能函数    空闲接收中断

#else
  USART_ITConfig(UART4, USART_IT_RXNE,
                 ENABLE);  // 中断使能函数    USART_IT_RXNE接收中断
#endif
  // Usart2 NVIC配置
  //  DMA_ITConfig(DMA1_Stream2, DMA_IT_TC, ENABLE);
  NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  USART_Cmd(UART4, ENABLE);  // 串口使能函数

#if UART4_DMA_RX_EN
  MYDMA_Config(DMA1_Stream2, DMA_Channel_4, (u32)&UART4->DR, (u32)UAT4_Rx_Buff,
               UAT4_Data_Length, (u32)0x00000000);
  USART_DMACmd(UART4, USART_DMAReq_Rx, ENABLE);  // 使能串口1的DMA接收

  DMA_Cmd(DMA1_Stream2, ENABLE);  // 关闭DMA传输
#endif
#if UART4_DMA_TX_EN
  MYDMA_Config(DMA1_Stream4, DMA_Channel_4, (u32)&UART4->DR,
               (u32)UAT4_TEXT_TO_SEND, 5, (u32)0x00000040);
#endif
}

void UART4_IRQHandler(void) {
#if UART4_DMA_RX_EN

  uart4_dma_rx();
#else

  uart4_rx();
#endif
}

// 开启一次DMA传输
// DMA_Streamx:DMA数据流,DMA1_Stream0~7/DMA2_Stream0~7
// ndtr:数据传输量
#if UART4_DMA_TX_EN
void USA4_DMA_Enable(DMA_Stream_TypeDef *DMA_Streamx, u16 ndtr) {
  DMA_Cmd(DMA_Streamx, DISABLE);                 // 关闭DMA传输
  USART_DMACmd(UART4, USART_DMAReq_Tx, ENABLE);  // 使能串口1的DMA发送
  while (DMA_GetCmdStatus(DMA_Streamx) != DISABLE)
    ;                                         // 确保DMA可以被设置
  DMA_SetCurrDataCounter(DMA_Streamx, ndtr);  // 数据传输量
  DMA_Cmd(DMA_Streamx, ENABLE);               // 开启DMA传输  启动传输
  //  printf("标志位=%d", DMA_GetFlagStatus(DMA2_Stream7, DMA_FLAG_TCIF7));
  while (DMA_GetFlagStatus(DMA_Streamx, DMA_FLAG_TCIF4) != SET)
    ;  // 等待DMA2_Steam7传输完成  传输完成时标志位为1
  DMA_Cmd(DMA_Streamx, DISABLE);               // 关闭DMA传输
  DMA_ClearFlag(DMA_Streamx, DMA_FLAG_TCIF4);  // 清除DMA2_Steam7传输完成标志
}
#endif

#if UART4_DMA_RX_EN
/**
 * @brief 通过DMA接收UART4的数据，并处理接收到的IMU数据
 * 
 * 该函数不接受参数，也不返回值。
 * 它主要完成以下功能：
 * 1. 检查UART4是否完成数据接收。
 * 2. 如果接收完成，处理接收到的IMU数据。
 * 3. 更新DMA状态，为下一次接收做准备。
 */
void uart4_dma_rx(void) {
  imu_data_t Angle;  // 定义角度数据结构
  
  // 检查UART4是否处于空闲状态（即接收完成）
  if (USART_GetITStatus(UART4, USART_IT_IDLE) != RESET) {
    uint32_t len;
    
    // 清除UART4的空闲状态标志
    UART4->SR;
    UART4->DR;
    
    // 关闭DMA接收
    DMA_Cmd(DMA1_Stream2, DISABLE);
    
    // 计算实际接收到的数据长度
    len = UAT4_Data_Length - 
          DMA_GetCurrDataCounter(DMA1_Stream2);
    
    // 如果接收到的数据长度为预期的IMU数据长度
    if (len == 22) {
      // 校验数据的完整性
      if (UAT4_Rx_Buff[11] == 0x55 && UAT4_Rx_Buff[12] == 0x53) {
        // 从接收到的数据中解析出角度值
        Angle.X = (float)((UAT4_Rx_Buff[14] << 8) | UAT4_Rx_Buff[13]) / 32768 * 180;
        Angle.Y = (float)((UAT4_Rx_Buff[16] << 8) | UAT4_Rx_Buff[15]) / 32768 * 180;
        Angle.Z = (float)((UAT4_Rx_Buff[18] << 8) | UAT4_Rx_Buff[17]) / 32768 * 180;
        
        // 调整角度值范围在-180度到180度之间
        if (Angle.X > 180) Angle.X = Angle.X - 360;
        if (Angle.Y > 180) Angle.Y = Angle.Y - 360;
        if (Angle.Z > 180) Angle.Z = Angle.Z - 360;
        
        // 将解析出的角度值发送到IMU数据队列
        xQueueSendFromISR(Imu_Data_Queue, &Angle, 0);
      }
    }
    
    // 清除DMA传输完成的标志
    DMA_ClearFlag(DMA1_Stream2, DMA_FLAG_TCIF2);
    
    // 设置DMA接收缓冲区的大小，为下一次接收做准备
    DMA_SetCurrDataCounter(DMA1_Stream2, UAT4_Data_Length);
    
    // 启用DMA接收
    DMA_Cmd(DMA1_Stream2, ENABLE);
  }
}
#else
void uart4_rx(void) {
  static u8 seri_count = 0;
  u16 check_sum = 0;
  static u8 i;
  static u8 flag;

  if (USART_GetITStatus(UART4, USART_IT_RXNE) != RESET) {
    //    i=USART_ReceiveData(USART3);

    LED1 = !LED1;
    if (USART_ReceiveData(UART4) == 0x55) {
      flag = 1;
    }

    if (flag) {
      USART4_RX_BUF[seri_count++] = USART_ReceiveData(UART4);
      if (seri_count == UAT4_Data_Length) {
        if (USART4_RX_BUF[0] == 0x55) {
          for (i = 0; i < UAT4_Data_Length; i++) {
            check_sum += USART4_RX_BUF[i];
          }
          printf(" \r\n ");

          seri_count = 0;
          flag = 0;
          // statu->Data_status = READY;
          // printf("\r\n  %d",xTaskGetTickCount());
        }
      }
    }
    USART_ClearFlag(USART3, USART_FLAG_RXNE);
  }
}
#endif
