/*
 * @File Description:
 * @Version: 2.0
 * @Autor: deng
 * @Date: 2021-08-01 15:19:51
 * @LastEditors: deng
 * @LastEditTime: 2021-08-02 18:15:36
 */
#ifndef usart4_H
#define usart4_H

#include "main.h"

/***********   UART4   *********/
#define UART4_EN 1         //使能（1）/禁止（0）串口4使用
#define UART4_DMA_TX_EN 0  //使能（1）/禁止（0）串口DMA发送
#define UART4_DMA_RX_EN 1  //使能（1）/禁止（0）串口DMA接收

extern uint8_t UAT4_Rx_Buff[Rx_BUF_SIZE];       //接收数据缓冲区
extern uint8_t UAT4_TEXT_TO_SEND[Rx_BUF_SIZE];  //发送数据缓冲区
extern uint8_t JY_901;

#define UAT4_Data_Length 44

extern uint8_t USART4_RX_BUF[Rx_BUF_SIZE];  //双缓存接收数据缓冲区

void UART4_Init(uint32_t bound);
void uart4_dma_rx(void);
void uart4_rx(void);

void USA4_DMA_Enable(DMA_Stream_TypeDef *DMA_Streamx, uint16_t ndtr);
#endif
