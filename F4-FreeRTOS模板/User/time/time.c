/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 19:39:14
 * @FilePath: \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\time\time.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\time\time.c
 * @Description:
 */
#include "./time./time.h"

/**
  * @brief 初始化TIM7时钟
  * @param arr: 自动重载寄存器的值，决定定时器的周期
  * @param psc: 预分频器的值，用于调整定时器的时钟频率
  * @note 此函数初始化TIM7定时器，配置其周期、预分频器、计数模式和中断，
  *       并启用TIM7的时钟和中断。
  */

void TIM7_Init_Clock(u16 arr, u16 psc) {
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  // 启用TIM7的APB1时钟
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);

  // 配置TIM7的时间基础结构
  TIM_TimeBaseInitStructure.TIM_Period = arr;
  TIM_TimeBaseInitStructure.TIM_Prescaler = psc;
  TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;

  // 初始化TIM7定时器
  TIM_TimeBaseInit(TIM7, &TIM_TimeBaseInitStructure);

  // 启用TIM7的更新中断
  TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);

  // 配置TIM7中断，包括中断通道、抢占优先级、子优先级和使能中断
  NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  // 启用TIM7定时器
  TIM_Cmd(TIM7, ENABLE);
}
