/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 18:43:16
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\key\bsp_key.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\key\bsp_key.c
 * @Description:
 */

#include "bsp_key.h"

/**
 * @brief  配置按键用到的I/O口
 * @param  无
 * @retval 无
 */
void Key_GPIO_Config(void) {
  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOE,
                         ENABLE);  // 使能GPIOA,GPIOE时钟

  GPIO_InitStructure.GPIO_Pin =
      GPIO_Pin_4 | GPIO_Pin_11 | GPIO_Pin_13;         // KEY0 KEY1 对应引脚
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;        // 普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;  // 100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;        // 上拉
  GPIO_Init(GPIOE, &GPIO_InitStructure);              // 初始化GPIOE2,3,4

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;           // KEY0 KEY1 对应引脚
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;        // 普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;  // 100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;      // 上拉
  GPIO_Init(GPIOA, &GPIO_InitStructure);              // 初始化GPIOE2,3,4

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;           // KEY0 KEY1 对应引脚
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;        // 普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;  // 100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;        // 上拉
  GPIO_Init(GPIOF, &GPIO_InitStructure);
}

/**
 * @brief   检测是否有按键按下
 *	@param 	GPIOx:具体的端口, x可以是（A...K）
 *	@param 	GPIO_PIN:具体的端口位， 可以是GPIO_PIN_x（x可以是0...15）
 * @retval  按键的状态
 *		@arg KEY_ON:按键按下
 *		@arg KEY_OFF:按键没按下
 */
uint8_t Key_Scan(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin) {
  /*检测是否有按键按下 */
  if (GPIO_ReadInputDataBit(GPIOx, GPIO_Pin) == KEY_ON) {
    /*等待按键释放 */
    if (GPIO_ReadInputDataBit(GPIOx, GPIO_Pin) == KEY_ON)
      return KEY_ON;
    else
      return KEY_OFF;
  } else
    return KEY_OFF;
}

// 按键处理函数
// 返回按键值
// mode:0,不支持连续按;1,支持连续按;
// 0，没有任何按键按下
// 1，KEY0按下
// 2，KEY1按下
// 3，KEY2按下
//
// 注意此函数有响应优先级,KEY0>KEY1>KEY2>WK_UP!!
u8 KEY_Scan(u8 mode) {
  static u8 key_up = 1;  // 按键按松开标志
  if (mode) key_up = 1;  // 支持连按
  if (key_up && (KEY1_E3 == 1 || Key_Scan(GPIOF, GPIO_Pin_2) == 0 ||
                 KEY3_B12 == 0 || KEY4_B13 == 0)) {
    vTaskDelay(10);  // 去抖动
    key_up = 0;
    if (KEY1_E3 == 1)
      return 1;
    else if (Key_Scan(GPIOF, GPIO_Pin_2) == 0)
      return 2;
    else if (KEY3_B12 == 0)
      return 3;
    else if (KEY4_B13 == 0)
      return 4;
  } else if (KEY1_E3 == 0 && Key_Scan(GPIOF, GPIO_Pin_2) == 1 &&
             KEY3_B12 == 1 && KEY4_B13 == 1)
    key_up = 1;
  return 0;  // 无按键按下
}

// 循迹初始化函数
void xunji_int(void) {
  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);  // 使能GPIOB时钟

  GPIO_InitStructure.GPIO_Pin =
      GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_3;           // PB4、B5对应引脚
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;        // 普通输入模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;  // 100M
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;        // 上拉
  GPIO_Init(GPIOB, &GPIO_InitStructure);              // 初始化GPIOB4，5
}

void KEY_switch(void) {
  switch (KEY_Scan(0)) {
    case KEY1_E3_PRES:
      break;
    case KEY2_E4_PRES:
      mode++;
      if (mode > 5) mode = 0;
      break;
    case KEY3_B12_PRES:
      start_tesk = 1;
      break;
    case KEY4_B13_PRES:
      start_tesk = 0;
      break;
    case KEY5_B14_PRES:
      break;
    case KEY6_B15_PRES:
      break;
    default:
      break;
  }
}
/*********************************************END OF FILE**********************/
