/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 18:39:42
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\adc\adc.c
 * @Description:
 */
#include "./adc./adc.h"

__IO u16 ADC_DNAnumb[2];

void Adc_Init(void)  // ADC通道初始化
{
  GPIO_InitTypeDef GPIO_InitStructure;
  ADC_CommonInitTypeDef ADC_CommonInitStructure;
  ADC_InitTypeDef ADC_InitStructure;
  //	NVIC_InitTypeDef NVIC_InitStructure;
  DMA_InitTypeDef DMA_InitStructure;
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);  // DMA2时钟使能

  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

  DMA_DeInit(DMA2_Stream0);
  while (DMA_GetCmdStatus(DMA2_Stream0) != DISABLE) {
  }  // 等待DMA可配置

  /* 配置 DMA Stream */
  DMA_InitStructure.DMA_Channel = DMA_Channel_0;              // 通道选择
  DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)&ADC1->DR;  // DMA外设地址
  DMA_InitStructure.DMA_Memory0BaseAddr = (u32)&ADC_DNAnumb;  // DMA 存储器0地址
  DMA_InitStructure.DMA_DIR =
      (u32)DMA_DIR_PeripheralToMemory;   // 存储器到外设模式0x00000040
                                         // 外设到存储器0x00000000
  DMA_InitStructure.DMA_BufferSize = 2;  // 数据传输量
  DMA_InitStructure.DMA_PeripheralInc =
      DMA_PeripheralInc_Disable;  // 外设非增量模式  外设地址保持不变
  DMA_InitStructure.DMA_MemoryInc =
      DMA_MemoryInc_Enable;  // 存储器增量模式   存储器地址递增
  DMA_InitStructure.DMA_PeripheralDataSize =
      DMA_PeripheralDataSize_HalfWord;  // 外设数据长度:8位
  DMA_InitStructure.DMA_MemoryDataSize =
      DMA_MemoryDataSize_HalfWord;  // 存储器数据长度:8位
  DMA_InitStructure.DMA_Mode =
      DMA_Mode_Circular;  // 使用普通模式   （与循环模式对应）
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;     // 中等优先级
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;  // 禁止FIFO模式
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;  // FIFO阈值
  DMA_InitStructure.DMA_MemoryBurst =
      DMA_MemoryBurst_Single;  // 存储器突发单次传输   单次传输
  DMA_InitStructure.DMA_PeripheralBurst =
      DMA_MemoryBurst_Single;  // 外设突发单次传输  单次传输
  DMA_Init(DMA2_Stream0, &DMA_InitStructure);  // 初始化DMA Stream

  // GPIOF9,F10初始化设置
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;        // 普通输出模式
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;  // 100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;    // 上拉
  GPIO_Init(GPIOA, &GPIO_InitStructure);              // 初始化

  // GPIOF9,F10初始化设置
  //  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
  //  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;	   //普通输出模式
  //  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz; //100MHz
  //  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;   //上拉
  //  GPIO_Init(GPIOC, &GPIO_InitStructure);			   //初始化

  // ADC_DeInit();

  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
  ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
  ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div4;
  ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
  ADC_CommonInit(&ADC_CommonInitStructure);

  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;  // 右对齐
  ADC_InitStructure.ADC_ExternalTrigConvEdge =
      ADC_ExternalTrigConvEdge_None;  // 禁止触发检测，使用软件触发
  ADC_InitStructure.ADC_NbrOfConversion =
      2;  // 1个转换在规则序列中，也就是只转换规则序列1
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;  // 分辨率为12位
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;      // 关闭连续转换
  ADC_InitStructure.ADC_ScanConvMode = ENABLE;  // 扫描模式为非扫描模式
  ADC_Init(ADC1, &ADC_InitStructure);

  // DMA_ITConfig(DMA2_Stream0, DMA_IT_TC, ENABLE);
  // NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  // NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
  // NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
  // NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  // NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // NVIC_Init(&NVIC_InitStructure);

  ADC_RegularChannelConfig(ADC1, ADC_Channel_5, 1, ADC_SampleTime_480Cycles);
  ADC_RegularChannelConfig(ADC1, ADC_Channel_6, 2, ADC_SampleTime_480Cycles);
  // ADC_RegularChannelConfig(ADC1, ADC_Channel_14, 3,
  // ADC_SampleTime_480Cycles);
  // ADC_RegularChannelConfig(ADC1, ADC_Channel_15, 4,
  // ADC_SampleTime_480Cycles);

  ADC_DMACmd(ADC1, ENABLE);

  ADC_Cmd(ADC1, ENABLE);

  DMA_Cmd(DMA2_Stream0, ENABLE);  // DMA传输

  ADC_SoftwareStartConv(ADC1);  // 软件开启转换

  ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);
  // ADC_DMACmd(ADC1, ENABLE); //使能的DMA
}

// void DMA2_Stream0_IRQHandler(void)
// {
// 	if (DMA_GetFlagStatus(DMA2_Stream0, DMA_FLAG_TCIF0) != 0)
// 	{
// 		printf("\r\n                    电压值=%d", ADC_DNAnumb);
// 		DMA_ClearFlag(DMA2_Stream0, DMA_FLAG_TCIF0);
// 		// DMA_ClearITPendingBit(DMA2_Stream0, DMA_IT_TCIF0);
// 	}
// }
u16 Get_Adc(u8 ch)  // 获得某个通道值
{
  ADC_RegularChannelConfig(ADC1, ch, 1, ADC_SampleTime_480Cycles);
  ADC_SoftwareStartConv(ADC1);  // 软件开启转换

  while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == 0)
    ;  // 一直在等待转换结束，转换结束后值为1

  return ADC_GetConversionValue(ADC1);  // 获取转换结果
}

u16 Get_Adc_Average(u8 ch, u8 times)  // 得到某个通道给定次数采样的平均值
// time的值为多少就是几次的平均值
{
  u32 temp_val = 0;
  u8 t;
  for (t = 0; t < times; t++) {
    temp_val += Get_Adc(ch);
    vTaskDelay(5);
  }
  return temp_val / times;  // 返回平均值
}
