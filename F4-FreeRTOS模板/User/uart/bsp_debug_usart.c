/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 19:44:57
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\uart\bsp_debug_usart.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\uart\bsp_debug_usart.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\uart\bsp_debug_usart.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\uart\bsp_debug_usart.c
 * @Description:
 */

#include "bsp_debug_usart.h"

/***********   USART1   *********/
u8 USART_RX_BUF[USART_REC_LEN];
u16 USART_RX_STA = 0;

/**
 * @brief 配置USART调试接口
 *
 * 该函数用于初始化USART1，配置GPIO引脚、USART参数，并开启USART1接收中断。
 *
 * @param bound USART的波特率
 */
void Debug_USART_Config(int bound) {
  // 初始化GPIO结构体
  GPIO_InitTypeDef GPIO_InitStructure;
  // 初始化USART结构体
  USART_InitTypeDef USART_InitStructure;
  // 初始化NVIC结构体
  NVIC_InitTypeDef NVIC_InitStructure;

  // 启用GPIOA和USART1的时钟
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

  // 配置GPIOA的9号和10号引脚为USART1功能
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

  // 配置GPIOA的9号和10号引脚为复用推挽输出，上拉，速度50MHz
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  // 配置USART1参数
  USART_InitStructure.USART_BaudRate = bound;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl =
      USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART1, &USART_InitStructure);

  // 启用USART1
  USART_Cmd(USART1, ENABLE);

  // 启用USART1接收中断
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

  // 配置USART1中断，设置优先级
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/**
 * @brief 配置DMA通道
 *
 * 该函数用于初始化并配置指定的DMA流，包括设置通道、外设地址、内存地址、数据传输量、传输方向等参数。
 *
 * @param DMA_Streamx 指向DMA流的指针，可以是DMA1或DMA2的任一流。
 * @param chx DMA通道选择。
 * @param par 外设地址，即数据从或发送到的外设地址。
 * @param mar 内存地址，即数据读取或写入的内存地址。
 * @param ndtr 数据传输计数，表示要传输的数据块数量。
 * @param Dir 数据传输方向，0表示从外设到内存，非0表示从内存到外设。
 */
void MYDMA_Config(DMA_Stream_TypeDef *DMA_Streamx, uint32_t chx, uint32_t par,
                  uint32_t mar, uint16_t ndtr, uint32_t Dir) {
  DMA_InitTypeDef DMA_InitStructure;

  // 根据DMA流的选择，启用相应的DMA时钟
  if ((uint32_t)DMA_Streamx > (uint32_t)DMA2) {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
  } else {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
  }

  // 初始化DMA流
  DMA_DeInit(DMA_Streamx);
  while (DMA_GetCmdStatus(DMA_Streamx) != DISABLE) {
  }  // 等待DMA流禁用完成

  // 配置DMA传输参数
  /* 设置 DMA Stream 的配置 */
  DMA_InitStructure.DMA_Channel = chx;
  DMA_InitStructure.DMA_PeripheralBaseAddr = par;
  DMA_InitStructure.DMA_Memory0BaseAddr = mar;
  DMA_InitStructure.DMA_DIR = Dir;
  DMA_InitStructure.DMA_BufferSize = ndtr;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(DMA_Streamx, &DMA_InitStructure);  // 应用配置并初始化DMA流
}

/**
  * @brief  向指定的USART发送一个字节数据。
  * @param  pUSARTx: 指向USART外设的指针。
  *         ch: 需要发送的数据字节。
  * @retval 无返回值。
  */
void Usart_SendByte(USART_TypeDef *pUSARTx, uint8_t ch) {
  /* 将字节数据发送到USART */
  USART_SendData(pUSARTx, ch);

  /* 等待USART发送完成 */
  while (USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET)
    ;
}

/*
 * 功能：通过USART发送字符串
 * 参数：
 *    pUSARTx：指向USART外设的指针，用于指定要操作的USART
 *    str：要发送的字符串的地址
 * 返回值：无
 */
void Usart_SendString(USART_TypeDef *pUSARTx, char *str) {
  unsigned int k = 0;  // 初始化计数器k为0

  // 循环发送字符串中的每个字符，直到遇到字符串结束符'\0'
  do {
    Usart_SendByte(pUSARTx, *(str + k));  // 发送当前字符
    k++;                                  // 移动到下一个字符
  } while (*(str + k) != '\0');

  // 等待传输完成
  while (USART_GetFlagStatus(pUSARTx, USART_FLAG_TC) == RESET) {
  }
}

/*********************
 * 功能：向指定的USART发送一个半字（16位数据）
 * 参数：
 *    pUSARTx：指向USART外设的指针
 *    ch：要发送的16位数据
 * 返回值：无
*********************/
void Usart_SendHalfWord(USART_TypeDef *pUSARTx, uint16_t ch) {
  uint8_t temp_h, temp_l;

  /* 分离出高8位和低8位 */
  temp_h = (ch & 0XFF00) >> 8;  // 获取高8位
  temp_l = ch & 0XFF;           // 获取低8位

  /* 发送高8位数据 */
  USART_SendData(pUSARTx, temp_h);
  /* 等待TXE标志位，确保数据被发送完 */
  while (USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET)
    ;

  /* 发送低8位数据 */
  USART_SendData(pUSARTx, temp_l);
  /* 等待TXE标志位，确保数据被发送完 */
  while (USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET)
    ;
}

/**
 * printf重定向函数。
 *
 * @param ch 需要写入的字符，类型为int。
 * @param f 指向文件流的指针，这里特指USART1的文件流。
 * @return 函数总是返回写入的字符，即参数ch的值。
 */
int fputc(int ch, FILE *f) {
  USART_SendData(USART1, (uint8_t)ch);  // 将字符ch发送到USART1

  /* 等待 Transmit Complete 旗标被置位，确保字符已被发送完毕 */
  while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
    ;

  return (ch);
}

/**
 * getchar函数。
 *
 * @param f 指向FILE类型的指针，表示要从中读取字符的文件流。
 * @return 返回从文件流中读取的字符，如果发生错误则返回EOF（文件结束符）。
 */
int fgetc(FILE *f) {
  /* 等待USART1接收缓冲区非空 */
  while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET)
    ;

  return (int)USART_ReceiveData(USART1);  // 从USART1接收数据并返回
}
/*********************************************END OF FILE**********************/
