/*
 *
 *    ┏┓　　　┏┓
 *  ┏┛┻━━━┛┻┓
 *  ┃　　　　　　　┃
 *  ┃　　　━　　　┃
 *  ┃　＞　　　＜　┃
 *  ┃　　　　　　　┃
 *  ┃...　⌒　...　┃
 *  ┃　　　　　　　┃
 *  ┗━┓　　　┏━┛
 *      ┃　　　┃　
 *      ┃　　　┃
 *      ┃　　　┃
 *      ┃　　　┃  神兽保佑
 *      ┃　　　┃  代码无bug　　
 *      ┃　　　┃
 *      ┃　　　┗━━━┓
 *      ┃　　　　　　　┣┓
 *      ┃　　　　　　　┏┛
 *      ┗┓┓┏━┳┓┏┛
 *        ┃┫┫　┃┫┫
 *        ┗┻┛　┗┻┛
 */

/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 19:38:04
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\tesk\user_tesk.c
 * @Description:
 */
#include "./tesk./user_tesk.h"

int mode = 0, tesk_num = 0, tesk_time = 0, tesk_time1 = 0, tesk_start_tesk = 0;
bool start_tesk = false, tesk_flag = false, bool_flag = false;
int dw_flag = 0;

/**
 * @brief
 * KEY_Task函数是一个永不停止的循环任务，主要用于OLED屏幕显示相关信息。
 *
 * @param parameter 该函数参数未使用，可传入NULL。
 */
void KEY_Task(void *parameter) {
  while (1) {
    // 该代码段用于延时，使得任务不会过快执行，间隔100个tick
    vTaskDelay(100); /* ��ʱ20��tick */

    // TODO: 添加OLED显示代码
    oled_data_t data;
    xQueueReceive(Oled_Data_Queue, &data, portMAX_DELAY);

    // 在OLED屏幕上显示相关信息
    OLED_ShowString(1, 1, "mode:");
    OLED_ShowString(2, 1, "tesk_num:");
    OLED_ShowNum(1, 6, mode, 3);
    OLED_ShowNum(2, 10, tesk_num, 3);
    OLED_ShowString(3, 1, "X:");
    OLED_ShowString(4, 1, "Y:");
    OLED_ShowNum(3, 3, data.tw1000_data.x, 4);
    OLED_ShowNum(4, 3, data.tw1000_data.y, 4);
    OLED_ShowString(3, 9, "X:");
    OLED_ShowString(4, 9, "Y:");
    OLED_ShowNum(3, 11, data.tof_data.X_data, 4);
    OLED_ShowNum(4, 11, data.tof_data.Y_data, 4);
  }
}

/**
 * 测试任务函数
 *
 * 本函数是一个无限循环的任务，主要功能是控制电机的运行。根据全局变量`start_tesk`的值，
 * 执行不同的操作：当`start_tesk`为真时，调用`moto_control()`函数控制电机；否则，停止电机控制，
 * 并通过设置TIM8的比较寄存器值为0来实现。
 *
 * @param parameter 传递给任务函数的参数，本函数中未使用。
 */
void Test_Task(void *parameter) {
  while (1) {
    if (start_tesk) {  // 当start_tesk为真时，执行电机控制
      moto_control();
    } else {                     // 否则，停止电机控制
      TIM_SetCompare1(TIM8, 0);  // 设置TIM8的比较器1为0
      TIM_SetCompare2(TIM8, 0);  // 设置TIM8的比较器2为0
      TIM_SetCompare3(TIM8, 0);  // 设置TIM8的比较器3为0
      TIM_SetCompare4(TIM8, 0);  // 设置TIM8的比较器4为0
      // 以上操作用于停止电机，通过设置TIM8的比较值为0，不再向电机发送控制信号
    }
    Time_Reset++;    // 时间重置计数器递增
    vTaskDelay(10);  // 任务延时，单位为tick
  }
}

/**
 *
 * 本函数用于不断循环执行测试控制逻辑和检查按键状态，并加入适当的延时。
 *
 * @param parameter 参数指针，本函数未使用该参数。
 */
void Test_date(void *parameter) {
  while (1) {        // 无限循环，用于持续执行测试逻辑
    Test_control();  // 执行测试控制逻辑
    KEY_switch();    // 检查按键状态
    vTaskDelay(50);  // 加入50毫秒的延时，以便于下一个循环的执行
  }
}

void Test_control(void) {
  // 坐标设置
  if (0 == mode) {
    CT_data_t data;
    xQueuePeek(CT_Queue, &data, NULL);
    place_x = set_place[tesk_num][0];
    place_y = set_place[tesk_num][1];
    place_z = set_place[tesk_num][2];
    if ((data.tw1000_data.x >= set_place[tesk_num][0] - 10 &&
         data.tw1000_data.x <= set_place[tesk_num][0] + 10) &&
        (data.tw1000_data.y >= set_place[tesk_num][1] - 10 &&
         data.tw1000_data.y <= set_place[tesk_num][1] + 10)) {
      if (data.imu_data.Z >= set_place[tesk_num][2] - 3 &&
          data.imu_data.Z <= set_place[tesk_num][2] + 3) {
        tesk_num++;
      }
    }
    if (tesk_num > 6) {
      tesk_num = 6;
    }
    // 是否到达目标位置
    if ((X_BUF >= 95 && X_BUF <= 115) && (Y_BUF >= 55 && Y_BUF <= 80) &&
        bool_flag == false)
      tesk_flag = true;
    else
      tesk_flag = false;
    if (tesk_flag) {
      // 到达目标点之后拣球的流程
      TIM_SetCompare1(TIM14, 1800);  // 1280
      TIM_SetCompare2(TIM3, 620);    // 680
      vTaskDelay(1500);
      tesk_flag = false;
      bool_flag == true;
      TIM_SetCompare1(TIM14, 1300);  // 1280
      TIM_SetCompare2(TIM3, 1200);   // 680
      vTaskDelay(1500);
      bool_flag == false;
    } else {
      TIM_SetCompare1(TIM14, 1300);  // 1280
      TIM_SetCompare2(TIM3, 1200);   // 680
    }
  }
}
