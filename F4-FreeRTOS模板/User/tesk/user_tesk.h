/***
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 18:37:45
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\tesk\user_tesk.h
 * @Description:
 */
#ifndef __tesk_H
#define __tesk_H

#include "main.h"
extern int mode;
extern bool start_tesk;
void KEY_Task(void* parameter);
void Test_Task(void* parameter);
void Test_control(void);
#endif
