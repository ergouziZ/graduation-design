/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 18:43:41
 * @FilePath: \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\led\bsp_led.c
 * @Description: 
 */


#include "bsp_led.h"

/**
 * @brief  初始化控制LED的IO
 * @param  无
 * @retval 无
 */
void LED_GPIO_Config(void)
{
  /*定义一个GPIO_InitTypeDef类型的结构体*/
  GPIO_InitTypeDef GPIO_InitStructure;

  /*开启LED相关的GPIO外设时钟*/
  RCC_AHB1PeriphClockCmd(LED1_GPIO_CLK |
                             LED2_GPIO_CLK |
                             RCC_AHB1Periph_GPIOC,
                         ENABLE);

  /*选择要控制的GPIO引脚*/
  GPIO_InitStructure.GPIO_Pin = LED1_PIN;

  /*设置引脚模式为输出模式*/
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;

  /*设置引脚的输出类型为推挽输出*/
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;

  /*设置引脚为上拉模式*/
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

  /*设置引脚速率为2MHz */
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

  /*调用库函数，使用上面配置的GPIO_InitStructure初始化GPIO*/
  GPIO_Init(LED1_GPIO_PORT, &GPIO_InitStructure);

  /*选择要控制的GPIO引脚*/
  GPIO_InitStructure.GPIO_Pin = LED2_PIN;
  GPIO_Init(LED2_GPIO_PORT, &GPIO_InitStructure);

  /*选择要控制的GPIO引脚*/
  GPIO_InitStructure.GPIO_Pin = LED3_PIN;
  GPIO_Init(LED3_GPIO_PORT, &GPIO_InitStructure);

  /*选择要控制的GPIO引脚*/
  GPIO_InitStructure.GPIO_Pin = LED4_PIN;
  GPIO_Init(LED4_GPIO_PORT, &GPIO_InitStructure);

  /*选择要控制的GPIO引脚*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  /*选择要控制的GPIO引脚*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  /*选择要控制的GPIO引脚*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  /*选择要控制的GPIO引脚*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  /*关闭RGB灯*/
  LED_RGBOFF;
}
/*********************************************END OF FILE**********************/
