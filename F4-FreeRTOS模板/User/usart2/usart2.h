/***
 * @Author: your name
 * @Date: 2024-05-12 22:06:11
 * @LastEditTime: 2024-05-12 23:23:39
 * @LastEditors: your name
 * @Description:
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart2\usart2.h
 * @可以输入预定的版权声明、个性签名、空行等
 */
/*
 * @File Description:
 * @Version: 2.0
 * @Autor: deng
 * @Date: 2021-08-01 15:19:51
 * @LastEditors: deng
 * @LastEditTime: 2021-08-02 18:15:36
 */
#ifndef usart2_H
#define usart2_H

#include "main.h"

/***********   USART2   *********/
#define USART2_EN 1  // ʹ�ܣ�1��/��ֹ��0������3ʹ��

#define USART2_DMA_RX_EN 1  // ʹ�ܣ�1��/��ֹ��0������DMA����

extern uint16_t USART2_RX_BUF[Rx_BUF_SIZE];  // ˫����������ݻ�����
extern uint8_t USA2_Rx[Rx_BUF_SIZE];  // �������ݻ�����
extern float X_BUF, Y_BUF;
#define USART2_Len 300

void USART2_Init(uint32_t bound);
void usart2_dma_rx(void);

#endif
