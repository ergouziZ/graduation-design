/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:11
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 19:48:17
 * @FilePath:
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart2\usart2.c
 * \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\usart2\usart2.c
 * @Description:
 */

#include "./usart2./usart2.h"

/***********   USART2   *********/
u16 USART2_RX_BUF[Rx_BUF_SIZE];
u8 USA2_Rx[Rx_BUF_SIZE];
float X_BUF = 0, Y_BUF = 0;
/**
 * 初始化USART2
 * @param bound 波特率，即USART的数据传输速率
 */
void USART2_Init(uint32_t bound) {
  // GPIO和USART初始化结构体声明
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  // 启用GPIOA和USART2的时钟
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

  // 配置GPIO Pin复用功能，用于USART2
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

  // 初始化GPIOA的PIN2，用于USART2的TX
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  // 初始化GPIOA的PIN3，用于USART2的RX
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  // 初始化USART2的参数
  USART_InitStructure.USART_BaudRate = bound;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl =
      USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART2, &USART_InitStructure);

// 如果使能了USART2的DMA接收，配置Idle状态中断
#if USART2_DMA_RX_EN
  USART_ITConfig(USART2, USART_IT_IDLE, ENABLE);
#endif

  // 启用USART2
  USART_Cmd(USART2, ENABLE);

  // 配置USART2中断，使能中断
  NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

// 如果使能了USART2的DMA接收，配置DMA
#if USART2_DMA_RX_EN
  MYDMA_Config(DMA1_Stream5, DMA_Channel_4, (u32)&USART2->DR, (u32)USA2_Rx,
               USART2_Len, (u32)0x00000000);
  USART_DMACmd(USART2, USART_DMAReq_Rx, ENABLE);
  DMA_Cmd(DMA1_Stream5, ENABLE);
#endif
}

/**
 * @brief 处理USART2的中断请求。
 *
 * 该函数在USART2接收到数据时被调用，用于处理DMA接收的情况。
 * 当USART2的DMA接收功能启用时（USART2_DMA_RX_EN定义为真），会调用usart2_dma_rx()函数
 * 进行DMA接收处理。
 *
 * @note 函数没有参数和返回值，因为它是作为中断服务例程设计的。
 */
void USART2_IRQHandler(void) {
#if USART2_DMA_RX_EN
  // 当USART2的DMA接收使能时，调用DMA接收处理函数
  usart2_dma_rx();
#endif
}

#if USART2_DMA_RX_EN
/**
 * @brief This function handles the USART2 receive process via DMA.
 * It checks for the Idle interrupt status of USART2, processes the received
 * data
 * if it's of the expected length (8 bytes), and updates the TOF (Time of
 * Flight) data queue.
 * No parameters are needed and the function does not return any value.
 */
void usart2_dma_rx(void) {
  // Check if the USART2 Idle interrupt has occurred
  if (USART_GetITStatus(USART2, USART_IT_IDLE) != RESET) {
    uint32_t len;
    tof_data_t tof_data_ptr = {0};

    // Clear the status registers to prevent double triggering
    USART2->SR;
    USART2->DR;

    // Disable the DMA to prevent interference while processing
    DMA_Cmd(DMA1_Stream5, DISABLE);

    // Calculate the length of the received data
    len = USART2_Len - DMA_GetCurrDataCounter(DMA1_Stream5);

    // If the received data length is exactly 8 bytes, process it
    if (len == 8) {
      // Extract and convert X and Y data from the received bytes
      tof_data_ptr.X_data = (float)(USA2_Rx[2] + (USA2_Rx[3] * 256));
      tof_data_ptr.Y_data = (float)(USA2_Rx[4] + (USA2_Rx[5] * 256));
      // Extract the red flag byte
      tof_data_ptr.red_flag = USA2_Rx[6];

      // Send the processed TOF data to the ISR queue for further handling
      xQueueSendFromISR(Tof_Data_Queue, &tof_data_ptr, NULL);
    }

    // Clear the DMA Transfer Complete Flag
    DMA_ClearFlag(DMA1_Stream5, DMA_FLAG_TCIF5);

    // Re-enable the DMA for continuous reception
    DMA_Cmd(DMA1_Stream5, ENABLE);
  }
}
#endif
