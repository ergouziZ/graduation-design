/*
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:06:10
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 18:40:03
 * @FilePath: \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\F4-FreeRTOS模板\User\encoder\encoder.c
 * @Description: 
 */
#include "./encoder./encoder.h"



Encoder_test Encoder_L;
Encoder_test Encoder_R;


void Encoder_Init(void)         //编码器相关外设初始化  
{
	TIM4_Mode_Config();
	TIM2_Mode_Config();
	TIM5_Int_Init(8400-1); 
}

void TIM4_Mode_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_ICInitTypeDef TIM_ICInitStructure; 
	NVIC_InitTypeDef NVIC_InitStruct;
	
	//PB6 ch1  A,PB7 ch2 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);//使能TIM4时钟	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);//使能GPIOA时钟
	
	
	GPIO_PinAFConfig(GPIOB,GPIO_PinSource6,GPIO_AF_TIM4);
	GPIO_PinAFConfig(GPIOB,GPIO_PinSource7,GPIO_AF_TIM4);
	
	GPIO_StructInit(&GPIO_InitStructure);//将GPIO_InitStruct中的参数按缺省值输入
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;         
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;//PB6 PB7浮空输入	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);                           

	TIM_DeInit(TIM4);//这个函数的意思是把TIM的寄存器值恢复到复位状态，
	                 //其实可以进到函数里去看的，那个结构体的值都是默认的复位值。
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);//指向将被初始化的Tim_timebase inittyduf*结构的指针
	TIM_TimeBaseStructure.TIM_Period =65535;//设定计数器重装值
	TIM_TimeBaseStructure.TIM_Prescaler = 0; //TIM4时钟预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision =TIM_CKD_DIV1 ;//设置时钟分割 T_dts = T_ck_int	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //TIM向上计数 
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);              
                 
	TIM_EncoderInterfaceConfig(TIM4, TIM_EncoderMode_TI12, TIM_ICPolarity_Falling ,TIM_ICPolarity_Falling);//使用编码器模式3，上升下降都计数
	TIM_ICStructInit(&TIM_ICInitStructure);//将结构体中的内容缺省输入
	TIM_ICInitStructure.TIM_ICFilter = 10;  //选择输入比较滤波器 
	TIM_ICInit(TIM4, &TIM_ICInitStructure);//将TIM_ICInitStructure中的指定参数初始化TIM4
	
	NVIC_InitStruct.NVIC_IRQChannel=TIM4_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=0;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	
	NVIC_Init(&NVIC_InitStruct);
	
	TIM_ARRPreloadConfig(TIM4, ENABLE);//使能预装载
	TIM_ClearFlag(TIM4, TIM_FLAG_Update);//清除TIM4的更新标志位
	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);//运行更新中断
	//Reset counter
	TIM_SetCounter(TIM4,0x7fff);

	TIM_Cmd(TIM4, ENABLE);   //启动TIM4定时器

}


void TIM2_Mode_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_ICInitTypeDef TIM_ICInitStructure; 
	NVIC_InitTypeDef NVIC_InitStruct;
	
	//PA0 ch1  A,PA1 ch2 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);//使能TIM2时钟	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);//使能GPIOA时钟
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource0,GPIO_AF_TIM2);
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource1,GPIO_AF_TIM2);

	
	GPIO_StructInit(&GPIO_InitStructure);//将GPIO_InitStruct中的参数按缺省值输入
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;         
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;//PA0 PA1浮空输入	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);                           

	TIM_DeInit(TIM2);//这个函数的意思是把TIM的寄存器值恢复到复位状态，
	                 //其实可以进到函数里去看的，那个结构体的值都是默认的复位值。
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);//指向将被初始化的Tim_timebase inittyduf*结构的指针
	TIM_TimeBaseStructure.TIM_Period =65535;//设定计数器重装值
	TIM_TimeBaseStructure.TIM_Prescaler = 0; //TIM2时钟预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision =TIM_CKD_DIV1 ;//设置时钟分割 T_dts = T_ck_int	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //TIM向上计数 
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);              
                 
	TIM_EncoderInterfaceConfig(TIM2, TIM_EncoderMode_TI12, TIM_ICPolarity_Falling ,TIM_ICPolarity_Falling);//使用编码器模式3，上升下降都计数
	TIM_ICStructInit(&TIM_ICInitStructure);//将结构体中的内容缺省输入
	TIM_ICInitStructure.TIM_ICFilter = 10;  //选择输入比较滤波器 
	TIM_ICInit(TIM2, &TIM_ICInitStructure);//将TIM_ICInitStructure中的指定参数初始化TIM2
	
	NVIC_InitStruct.NVIC_IRQChannel=TIM2_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=0;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	
	NVIC_Init(&NVIC_InitStruct);
	
	TIM_ARRPreloadConfig(TIM2, ENABLE);//使能预装载
	TIM_ClearFlag(TIM2, TIM_FLAG_Update);//清除TIM4的更新标志位
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);//运行更新中断
	//Reset counter
	TIM_SetCounter(TIM2,0x7fff);

	TIM_Cmd(TIM2, ENABLE);   //启动TIM4定时器

}



void TIM5_Int_Init(u32 psc)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5,ENABLE);
	
	TIM_TimeBaseInitStruct.TIM_Period=CYCLE_TIME-1;
	TIM_TimeBaseInitStruct.TIM_Prescaler=psc;
	TIM_TimeBaseInitStruct.TIM_CounterMode=TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_ClockDivision=TIM_CKD_DIV1;
	
	TIM_TimeBaseInit(TIM5,&TIM_TimeBaseInitStruct);
    
	TIM_ITConfig(TIM5,TIM_IT_Update,ENABLE);
	
	NVIC_InitStruct.NVIC_IRQChannel=TIM5_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=1;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	
	NVIC_Init(&NVIC_InitStruct);
	
	TIM_Cmd(TIM5,ENABLE);

}



#if LorR_CALCULATION

void L_calculation(void)
{
	TIM_Cmd(TIM4,DISABLE);      //关闭计数器	
	Encoder_L.cnt = TIM4->CNT;
	TIM4->CNT = 0x7fff;
	TIM_Cmd(TIM4,ENABLE);     //开启计数器
	
	if(Encoder_L.cnt>0x7fff)
	{
		Encoder_L.angle = Encoder_L.cnt-0x7fff;  //获取计数值	
		Encoder_L.total_angle -= Encoder_L.angle;   //计算累计速度
		
		Encoder_L.rouud = Encoder_L.angle/TOTAL_RESOLUTION_two;    //计算当前周期转过的圈数 电机转动一圈产生的脉冲
		Encoder_L.round_cnt  -= Encoder_L.rouud;   //计数累计圈数
		
		Encoder_L.speed_rpm = -Encoder_L.rouud*ENCODER_TIME*MOTOR_REDUCTION_RATIO_two ;   //计算转速  圈/分
	}
	else
	{
		Encoder_L.angle = 0x7fff-Encoder_L.cnt;//获取计数值	
		Encoder_L.total_angle += Encoder_L.angle;
		
		Encoder_L.rouud = Encoder_L.angle/TOTAL_RESOLUTION_two;
		Encoder_L.round_cnt  += Encoder_L.rouud;
		
		Encoder_L.speed_rpm = Encoder_L.rouud*ENCODER_TIME*MOTOR_REDUCTION_RATIO_two ;   //计算转速  圈/分
	}	
	
	
	
}

void R_calculation(void)
{
	TIM_Cmd(TIM2,DISABLE);
	Encoder_R.cnt = TIM2->CNT;
	TIM2->CNT = 0x7fff;
	TIM_Cmd(TIM2,ENABLE);
	
	if(Encoder_R.cnt > 0x7fff)
	{
		Encoder_R.angle = Encoder_R.cnt-0x7fff;//获取计数值	
		Encoder_R.total_angle -= Encoder_R.angle;   //计算累计速度
		
		Encoder_R.rouud = Encoder_R.angle/TOTAL_RESOLUTION_two;    //计算当前周期转过的圈数 电机转动一圈产生520个跳变沿
		Encoder_R.round_cnt  -= Encoder_R.rouud;   //计数累计圈数
		
		Encoder_R.speed_rpm = -Encoder_R.rouud*ENCODER_TIME*MOTOR_REDUCTION_RATIO_two ;   //计算转速  圈/分
	}
	else
	{
		Encoder_R.angle = 0x7fff - Encoder_R.cnt;//获取计数值	
		Encoder_R.total_angle += Encoder_R.angle;   //计算累计速度
		
		Encoder_R.rouud = Encoder_R.angle/TOTAL_RESOLUTION_two;    //计算当前周期转过的圈数 电机转动一圈产生520个跳变沿
		Encoder_R.round_cnt  += Encoder_R.rouud;   //计数累计圈数
		
		Encoder_R.speed_rpm = Encoder_R.rouud*ENCODER_TIME*MOTOR_REDUCTION_RATIO_two ;   //计算转速  圈/分
	
	}
}

#endif

#if CALCULATION
void calculation(Encoder_test* Encoder,TIM_TypeDef* TIMx)
{
	TIM_Cmd(TIMx,DISABLE);
	Encoder->cnt = TIMx->CNT;
	TIM_SetCounter(TIMx,0x7fff);
	TIM_Cmd(TIMx,ENABLE);
	
	/*正转*/
	if(Encoder->cnt > 0x7fff)
	{
		Encoder->current_count = Encoder->cnt-0x7fff;//获取当前时段计数值	
		Encoder->Total_count += Encoder->current_count;   //计算累计计数值
		
		Encoder->rouud = Encoder->current_count/TOTAL_RESOLUTION_two;    //计算当前周期电机输出轴转过的圈数
		Encoder->round_cnt  += Encoder->rouud;   //计数累计圈数
		
		Encoder->angle = Encoder->rouud*360.0f;
		Encoder->total_angle = Encoder->round_cnt*360.0f;
		
		/*计算转速 =当前周期转过的圈数*时间系数 圈/分*/
		Encoder->speed_rpm = Encoder->rouud*(float)ENCODER_TIME;   //计算转速  圈/分
	}
	/*反转*/
	else
	{
		Encoder->current_count = 0x7fff - Encoder->cnt;//获取计数值	
		Encoder->Total_count -= Encoder->current_count;   //计算累计数值
		
		Encoder->rouud = -Encoder->current_count/TOTAL_RESOLUTION_two;    //计算当前周期电机输出轴转过的圈数
		Encoder->round_cnt  += Encoder->rouud;   //计数累计圈数
		
		Encoder->angle = Encoder->rouud*360.0f; //电机输出轴当前周期转过的角度
		Encoder->total_angle = Encoder->round_cnt*360.0f; //电机输出轴累计转过的角度
		
		/*计算转速 =当前周期转过的圈数*时间系数 圈/分*/
		Encoder->speed_rpm = Encoder->rouud*(float)ENCODER_TIME;  
	
	}
}

#endif


/*-------------------------------------------定时器5中断服务函数--------------------------------------------------*/
void TIM5_IRQHandler(void)
{

	if(TIM_GetITStatus(TIM5,TIM_IT_Update)!=0)
	{
		
#if CALCULATION
		
		calculation(&Encoder_L,TIM4);
		calculation(&Encoder_R,TIM2);
		
#endif	
		
#if LorR_CALCULATION
		
		L_calculation();
		R_calculation();
		
#endif		
	}
	TIM_ClearITPendingBit(TIM5,TIM_IT_Update);
}



