#ifndef __ENCODER_H
#define __ENCODER_H
#include "main.h"

#define ENCODER_TIME 6000.0             /*时间系数注意要与定时器5中断周期匹配不然得出的值不准确（计算示例：1min/10ms = 6000 由此计算出的转速为：rpm/min）*/
#define CYCLE_TIME   199                 /*定时器5中断周期10ms计算一次*/
#define ENCODER_RESOLUTION_two 13.0    /*编码器一圈的物理脉冲数*/
#define ENCODER_MULTIPLE_two 4.0       /*编码器倍频，通过定时器的编码器模式设置*/
#define MOTOR_REDUCTION_RATIO_two 20 /*电机的减速比*/
/*电机转一圈总的脉冲数(定时器能读到的脉冲数) = 编码器物理脉冲数*编码器倍频*电机减速比 */
/* 11*4*34= 1496*/
#define TOTAL_RESOLUTION_two ( ENCODER_RESOLUTION_two*ENCODER_MULTIPLE_two*MOTOR_REDUCTION_RATIO_two )  


#define CALCULATION          1
#define LorR_CALCULATION     0


typedef struct 
{
    float	 	speed_rpm;    //转速（通过编码器计算后的速度）
	uint16_t 	cnt;	      //定时器计数器值
	float       current_count;//定时器计数器当前值
	float       Total_count;  //计数器计数总值
    float 		angle;		  //当前角度:[0,580]
	float  		rouud;        //当前圈数  
    float		round_cnt;    //累计圈数
    float		total_angle;  //累计角度

} Encoder_test;

extern Encoder_test Encoder_L;
extern Encoder_test Encoder_R;

void TIM5_IRQHandler(void);
void Encoder_Init(void);
void TIM4_Mode_Config(void);
void TIM2_Mode_Config(void);
void TIM5_Int_Init(uint32_t psc);
void L_calculation(void);
void R_calculation(void);
void calculation(Encoder_test* Encoder,TIM_TypeDef* TIMx);

#endif


