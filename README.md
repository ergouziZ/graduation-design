<!--
 * @Author: 二狗子 2030619016@hzxy.edu.cn
 * @Date: 2024-05-12 22:05:46
 * @LastEditors: 二狗子 2030619016@hzxy.edu.cn
 * @LastEditTime: 2024-05-16 20:30:25
 * @FilePath: \RVMDK（uv5）c:\Users\ergouzi\Desktop\毕业设计\毕业设计开源代码\README.md
 * @Description: 
-->
# 基于STM32的自动捡乒乓球球机器人

#### 介绍

该项目是本人的毕业设计题目，实现了小车的定点巡航、2.4GLORA遥控控制、自动识别并拾取乒乓球、全向运动等多种功能。  

本项目中使用的定位模块是维特智能的JY1000_SMA UWB室内定位模块，实测可实现精度为+-10cm的室内定位、室内导航等功能。[模块资料](https://wit-motion.yuque.com/wumwnr/docs/rhwm2dti9yi3kqt0)  

硬件部分：使用STM32F407ZGT6单片机，4个麦克纳姆轮 、4个UWB模块、2个JY901陀螺仪、按键若干。  

软件部分：

* FreeRTOS使用野火电子的Free RTOS模板例程。
  
* pid算法参考了这个开源项目的部分PID算法实现思路：[https://gitee.com/mhappy/RoboMaster-C.git](https://gitee.com/mhappy/RoboMaster-C.git)  
